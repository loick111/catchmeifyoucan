/** 
 * @file   client.cpp
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 20:54
 * @brief 
 */

#include <sys/socket.h>
#include "client.h"
#include "gameroom.h"
#include "log.h"
#include "packet.h"

bool Client::write(const SClient & client, const CVPacketData & data){
    if(send(client.sock, &data[0], data.size() * sizeof(packet_data), 0) == -1){
        Log::report("Write error", Log::FAIL);
        return false;
    }
    return true;
}

bool Client::asyncWrite(SClient& client, const CVPacketData& data){
    //client.mtx.lock();
    sem_wait(&client.mutex);
    if(client.queue.size() > KMaxSendQueueSize)
        return false;
    
    client.queue.reserve(client.queue.size() + data.size());
    
    for(packet_data p_data : data)
        client.queue.push_back(p_data);
    //client.mtx.unlock();
    sem_post(&client.mutex);
    return true;
}

bool Client::read(SClient& client, CVPacketData & data){
    int n;
    //packet_data data[KBufferSize];
    data.resize(KBufferSize);
    
    if((n = recv(client.sock, &data[0], data.size() * sizeof(packet_data), 0)) == -1){
        Log::report("Read error", Log::FAIL);
        return false;
    }
    
    data.resize(n);
    
    if(n == 0)
        return false;
    
    //Packet::parse(data, size, client);
    return true;
}
