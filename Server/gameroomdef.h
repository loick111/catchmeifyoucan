/** 
 * @file   gameroomdef.h
 * @author Vincent Quatrevieux
 *
 * @date 30 décembre 2013, 14:49
 * @brief 
 */

#ifndef GAMEROOMDEF_H
#define	GAMEROOMDEF_H

namespace GameRoom{
    typedef struct SGameRoom SGameRoom;
    typedef struct SPlayer SPlayer;
    typedef struct{
        short row;
        short col;
    } SPosition;
    typedef std::vector<unsigned> CVRow;
    typedef std::vector<CVRow> CVMap;
}

#endif	/* GAMEROOMDEF_H */

