/** 
 * @file   client.h
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:42
 * @brief handle the clients sockets
 */

#ifndef CLIENT_H
#define	CLIENT_H

#include <semaphore.h> //internal compiler error with mutex, using semaphore instead
#include "constants.h"
#include "clientdef.h"
#include "gameroomdef.h"

namespace Client{
    struct SClient{
        SOCKET sock;
        CVPacketData queue;
        //std::mutex mtx;
        sem_t mutex;
        GameRoom::SPlayer *player = nullptr;
        
        SClient(){
            sem_init(&mutex, 0, 1);
        }
    };
    
    bool write(const SClient & client, const CVPacketData & data);
    bool read(SClient & client, CVPacketData &data);
    bool asyncWrite(SClient & client, const CVPacketData & data);
}

#include "gameroom.h"

#endif	/* CLIENT_H */

