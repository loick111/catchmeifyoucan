/** 
 * @file   server.h
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:38
 * @brief base sever socket
 */

#ifndef SERVER_H
#define	SERVER_H

//#include <vector>
#include <list>
#include "constants.h"
#include "client.h"

using namespace Client;

namespace Server{
    typedef int SOCKET;
    
    typedef struct{
        SOCKET sock;
        //std::vector<SClient> clients;
        std::list<SClient> clients;
        SOCKET lastSock;
        bool running = false;
    } SServer;
    
    SServer init();
    void stop(SServer & srv);
    void run(SServer & srv);
    void removeClient(SServer & srv, Client::SClient & client);
}

#endif	/* SERVER_H */

