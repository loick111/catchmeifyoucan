#include "server.h"
#include "log.h"
#include <signal.h>

namespace{
    Server::SServer server;
    void sigHandler(int){
        Server::stop(server);
    }
}

int main(){
    Log::init();
    server = Server::init();
    
    signal(SIGTERM, sigHandler);
    signal(SIGINT, sigHandler);
    
    Server::run(server);
    return 0 ;
}