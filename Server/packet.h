/** 
 * @file   packet.h
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 21:01
 * @brief handle all packets
 */

#ifndef PACKET_H
#define	PACKET_H

#include <string>
#include <vector>
#include "client.h"
#include "constants.h"

namespace Packet{
    typedef struct SPacket SPacket;
    bool processHELLO(SPacket & packet, Client::SClient & client);
    bool processLIST(SPacket & packet, Client::SClient & client);
    bool processCREATE(SPacket & packet, Client::SClient & client);
    bool processJOIN(SPacket & packet, Client::SClient & client);
    bool processINFO(SPacket & packet, Client::SClient & client);
    bool processINIT(SPacket & packet, Client::SClient & client);
    bool processTURN(SPacket & packet, Client::SClient & client);
    bool processMOVE(SPacket & packet, Client::SClient & client);
    
    typedef bool (*process_callback)(SPacket & packet, Client::SClient & client);
    struct SPacket{
        packet_data id;
        std::string name;
        process_callback process;
        CVPacketData data;
    };
    
    const SPacket HELLO = {
        1,
        "HELLO",
        processHELLO
    };
    
    const SPacket LIST = {
        2,
        "LIST",
        processLIST
    };
    
    const SPacket CREATE = {
        3,
        "CREATE",
        processCREATE
    };
    
    const SPacket JOIN = {
        4,
        "JOIN",
        processJOIN
    };
    
    const SPacket INFO = {
        5,
        "INFO",
        processINFO
    };
    
    const SPacket INIT = {
        6,
        "INIT",
        processINIT
    };
    
    const SPacket TURN = {
        7,
        "TURN",
        processTURN
    };
    
    const SPacket MOVE = {
        8,
        "MOVE",
        processMOVE
    };
    
    const SPacket DEAD = {
        9,
        "DEAD"
    };
    
    const SPacket END = {
        10,
        "END"
    };
    
    const std::vector<SPacket> KPackets = {
        HELLO,
        LIST,
        CREATE,
        JOIN,
        INFO,
        INIT,
        TURN,
        MOVE
    };
    
    bool parse(const CVPacketData & data, Client::SClient & client);
    bool send(Client::SClient & client, const SPacket & packet, bool async = true);
    SPacket generate(const SPacket & model, packet_data data, const CVPacketData & extra = CVPacketData());
    packet_data getHeadData(const SPacket & packet);
    bool sendINFO(Client::SClient & client, unsigned id);
    void sendToGameRoom(const GameRoom::SGameRoom & room, const SPacket & packet);
    SPacket generateINFO(const GameRoom::SGameRoom & room);
    bool sendINIT(GameRoom::SGameRoom & room);
    void sendTURN(GameRoom::SGameRoom & room);
    void sendDEAD(GameRoom::SGameRoom & room, GameRoom::SPlayer & player);
    void sendEND(GameRoom::SGameRoom & room);
}

#endif	/* PACKET_H */

