/** 
 * @file   gameroom.h
 * @author Vincent Quatrevieux
 *
 * @date 28 décembre 2013, 15:06
 * @brief handle game rooms
 */

#ifndef GAMEROOM_H
#define	GAMEROOM_H

#include <vector>
#include <list>
#include "constants.h"
#include "clientdef.h"
#include "gameroomdef.h"

namespace GameRoom{    
    struct SGameRoom{
        unsigned short width;
        unsigned short height;
        unsigned short maxPlayers;
        bool init = false;
        unsigned id;
        std::vector<SPlayer*> players;
        unsigned short curPlayers = 0;
        bool accessible = true;
        CVMap map;
        unsigned short playerTurn = 0;
        unsigned death = 0;
        unsigned startTime;
        unsigned turnCount;
    }; 
    
    struct SPlayer{
        Client::SClient *client = nullptr;
        SGameRoom *gameRoom = nullptr;
        SPosition pos;
        bool ready = false;
        unsigned pid;
        bool alive = true;
    };
    
    std::list<SGameRoom> getGameRooms();
    SGameRoom & get(unsigned id);
    SGameRoom & create(unsigned short nbPlayers);
    void addPlayer(Client::SClient &client, SGameRoom & gameRoom);
    void removePlayer(SPlayer *player);
    void init(SGameRoom & room);
    bool isFull(const SGameRoom & room);
    void startTurn(SGameRoom & room);
    bool movePlayer(SGameRoom &room, unsigned direction);
    void killPlayer(SGameRoom & room, SPlayer & player);
    void removeGameRoom(SGameRoom & room);
    void playerLogout(SPlayer & player);
}

#include "client.h"

#endif	/* GAMEROOM_H */

