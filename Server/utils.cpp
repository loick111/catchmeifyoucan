/** 
 * @file   utils.cpp
 * @author Vincent Quatrevieux
 *
 * @date 1 janvier 2014, 16:34
 * @brief 
 */

#include <ctime>
#include <cstdlib>
#include "utils.h"

namespace{ //Private
    bool is_init = false;
}

unsigned Utils::random(unsigned min, unsigned max){
    if(!is_init){
        srand(time(NULL));
        is_init = true;
    }
    
    return (rand() % (max - min)) + min;
}
