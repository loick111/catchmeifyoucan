# Server app

This app handle the online game rooms and synchronize all clients (and maybe players).

## Packet system

The packet was organized by 2 parts : head and extradata.
All datas was encoded with one byte.

### Head

The head have 2 parts with same size, 4 bits.

The first 4 bits are the packet id (1 to 15).
The second part was the head data (between 0 to 15)

### extradata

The extradata are useful but not compulsory in quite some cases.
