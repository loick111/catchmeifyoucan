/** 
 * @file   server.cpp
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:38
 * @brief 
 */

#include <thread>
#include <semaphore.h>
#include <system_error>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include "server.h"
#include "log.h"
#include "constants.h"
#include "packet.h"

using namespace Server;

namespace{
    void inputHandler(SServer &server){
        while(server.running){
            fd_set rdfs;
            FD_ZERO(&rdfs); //clean rdfs

            FD_SET(server.sock, &rdfs); //adding server
            for(const SClient & client : server.clients){ //adding all clients
                FD_SET(client.sock, &rdfs);
            }
            
            struct timeval tv;
            tv.tv_sec = 2;
            tv.tv_usec = 0;
            
            int nb;
            if((nb = select(server.lastSock + 1, &rdfs, NULL, NULL, &tv)) < 0){
                Log::report("Select error !", Log::FAIL);
                break;
            }
            
            if(nb == 0){ //timeout (2sec)
                continue;
            }

            if(FD_ISSET(server.sock, &rdfs)){ //new client
                struct sockaddr_in csin;
                socklen_t csinSize = sizeof csin;
                SOCKET sock = accept(server.sock, (struct sockaddr*) &csin, &csinSize);

                if(sock == -1){
                    Log::report("Cannot accept new client !", Log::FAIL);
                    continue;
                }

                server.lastSock = server.lastSock > sock ? server.lastSock : sock;

                SClient client;
                client.sock = sock;

                Packet::send(client, Packet::HELLO);

                server.clients.push_back(client);
                Log::report("New client !");
            }else{
                for(SClient & client : server.clients){
                //for(unsigned i = 0; i < server.clients.size(); ++i){
                    //SClient & client = server.clients[i];
                    if(FD_ISSET(client.sock, &rdfs)){
                        CVPacketData data;
                        if(!Client::read(client, data) || !Packet::parse(data, client)){
                            Log::report("Logout...");
                            Server::removeClient(server, client);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    void outputHandler(SServer & server){
        while(server.running){
            fd_set wrfs;
            FD_ZERO(&wrfs);

            bool found = false;

            for(SClient& client : server.clients){
                if(!client.queue.empty()){
                    FD_SET(client.sock, &wrfs);
                    found = true;
                }
            }

            if(!found){ //no async packets to send
                usleep(10000); //10ms
                continue;
            }

            Log::report("Sending async packets...");

            if(select(server.lastSock + 1, NULL, &wrfs, NULL, NULL) < 0){
                Log::report("select write error", Log::FAIL);
                break;
            }

            for(SClient & client : server.clients){
                sem_wait(&client.mutex);
                if(FD_ISSET(client.sock, &wrfs)){
                    if(!Client::write(client, client.queue)){
                        Log::report("Logout...");
                        Server::removeClient(server, client);
                        break;
                    }else{
                        Log::report(std::to_string(client.queue.size()) + " bytes sent");
                        client.queue.clear();
                    }
                }
                sem_post(&client.mutex);
            }
        }
    }
}

SServer Server::init(){
    Log::report("Creating server...", Log::Level::INFO);
    SServer server;
    
    //server.clients.reserve(100);
    
    server.sock = socket(AF_INET, SOCK_STREAM, 0);
    
    if(server.sock == -1){
        Log::report("Unable to create the socket !", Log::Level::FAIL);
        return server;
    }
    
    server.lastSock = server.sock;
    
    struct sockaddr_in sin;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(KPort);
    sin.sin_family = AF_INET;
    
    if(bind(server.sock, (struct sockaddr *) &sin, sizeof sin) == -1){
        Log::report("Unable to bind socket !", Log::Level::FAIL);
        return server;
    }
    
    if(listen(server.sock, KMaxClients) == -1){
        Log::report("Unable tu listen port !", Log::Level::FAIL);
        return server;
    }
    
    Log::report("Server created !", Log::GOOD);
    return server;
}

void Server::stop(SServer& srv){
    Log::report("Stoping server...", Log::Level::INFO);
    srv.running = false;
    usleep(100000); //pause 100ms
    
    for(const SClient & client : srv.clients)
        close(client.sock);
    
    srv.clients.clear();
    
    close(srv.sock);
    srv.lastSock = 0;
    srv.sock = 0;
    
    Log::report("Server stoped", Log::Level::GOOD);
}

void Server::run(SServer& server){
    Log::report("Server running on port " + std::to_string(KPort), Log::Level::INFO);
    
    server.running = true;
    
    try{
        std::thread inputThread(inputHandler, std::ref(server));
        std::thread outputThread(outputHandler, std::ref(server));

        inputThread.join();
        outputThread.join();
    }catch(const std::exception &err){
        Log::report(err.what(), Log::FAIL);
    }catch(...){
        Log::report("An undefined error was uncountered", Log::FAIL);
    }
}

void Server::removeClient(SServer & srv, Client::SClient& client){
    auto it = srv.clients.begin();
    for(; it != srv.clients.end() && it->sock != client.sock; ++it);

    if(it == srv.clients.end())
        return;
    
    if(client.player != nullptr){
        /*GameRoom::SGameRoom &room = *client.player->gameRoom;
        if(room.accessible){
            GameRoom::removePlayer(client.player);
            client.player = nullptr;
            Packet::sendToGameRoom(room, Packet::generateINFO(room));
        }else{
            client.player->client = nullptr;
            GameRoom::killPlayer(room, *client.player);
            
            if(room.playerTurn == client.player->pid) //passe son tours
                GameRoom::startTurn(room);
        }*/
        GameRoom::playerLogout(*client.player);
        client.player = nullptr;
    }

    close(client.sock);
    client.sock = 0;
    client.queue.clear();
    sem_close(&client.mutex);
    sem_destroy(&client.mutex);
    srv.clients.erase(it);
}
