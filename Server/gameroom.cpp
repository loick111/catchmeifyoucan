/** 
 * @file   gameroom.cpp
 * @author Vincent Quatrevieux
 *
 * @date 28 décembre 2013, 15:06
 * @brief Handling game rooms
 */

#include <stdexcept>
#include <ctime>
#include "gameroom.h"
#include "constants.h"
#include "log.h"
#include "utils.h"
#include "packet.h"

using namespace GameRoom;

namespace{
    std::vector<SGameRoom> rooms(KMaxGameRooms);
    
    unsigned & getByPosition(CVMap & map, const SPosition & pos){
        return map[pos.row][pos.col];
    }
    
    enum Direction{
        UP = 1,
        LEFT = 2,
        DOWN = 4,
        RIGHT = 8
    };
}


std::list<SGameRoom> GameRoom::getGameRooms(){
    std::list<SGameRoom> list_rooms;
    
    for(SGameRoom & room : rooms){
        if(room.init)
            list_rooms.push_back(room);
    }
    
    return list_rooms;
}

SGameRoom & GameRoom::get(unsigned id){
    if(id > KMaxGameRooms || !rooms.at(id).init)
        throw std::runtime_error("Cannot found the GameRoom");
    
    return rooms[id];
}

SGameRoom & GameRoom::create(unsigned short nbPlayers){    
    unsigned id;
    for(id = 0; id < rooms.size() && rooms[id].init; ++id);
    
    if(id == rooms.size())
        throw std::runtime_error("Cannot create a new GameRoom");
    
    
    SGameRoom room;
    
    room.maxPlayers = nbPlayers;
    room.height = KMinHeight + (KMaxHeight - KMinHeight) * nbPlayers / KMaxPlayers;
    room.width = KMinWidth + (KMaxWidth - KMinWidth) * nbPlayers / KMaxPlayers;
    room.init = true;
    room.id = id;
    room.players.clear();
    room.players.reserve(nbPlayers);
    rooms[id] = room;
    
    return rooms[id];
}

void GameRoom::addPlayer(Client::SClient &client, SGameRoom& gameRoom){
    SPlayer *player = new SPlayer;
    player->client = &client;
    player->gameRoom = &gameRoom;
    
    gameRoom.players.push_back(player);
    ++gameRoom.curPlayers;
    
    client.player = player;
    
    if(GameRoom::isFull(gameRoom))
        GameRoom::init(gameRoom);
}

void GameRoom::removePlayer(SPlayer* player){
    std::vector<GameRoom::SPlayer*> &players = player->gameRoom->players;
    
    auto it = players.begin();
    for(;it != players.end() && (*it) != player; ++it);
    
    players.erase(it);
    delete player;
    --player->gameRoom->curPlayers;
}

void GameRoom::init(SGameRoom& room){
    room.map.resize(room.height);
    
    for(CVRow &row : room.map){
        row.resize(room.width);
    }
    
    unsigned pid = 1;
    for(SPlayer *player : room.players){
        SPosition pos;
        do{
            pos.col = Utils::random(0, room.width);
            pos.row = Utils::random(0, room.height);
        }while(getByPosition(room.map, pos) != 0);
        player->pos = pos;
        player->pid = pid++;
        getByPosition(room.map, player->pos) = player->pid;
    }
    
    room.accessible = false;
    room.startTime = time(NULL);
    room.turnCount = 1;
}

bool GameRoom::isFull(const SGameRoom& room){
    return room.curPlayers == room.maxPlayers;
}

void GameRoom::startTurn(SGameRoom& room){
    if((short)room.death == room.curPlayers - 1){
        room.playerTurn = 0;
        Packet::sendEND(room);
        GameRoom::removeGameRoom(room);
        return;
    }
    do{
        ++room.playerTurn;
        
        if(room.playerTurn > room.maxPlayers){ //all players has already played
            room.playerTurn = 1;
            ++room.turnCount;
        }
        
    }while(!room.players[room.playerTurn - 1]->alive);
    
    Packet::sendTURN(room);
}

bool GameRoom::movePlayer(SGameRoom& room, unsigned direction){
    SPlayer &player = *room.players[room.playerTurn - 1];
    SPosition target = player.pos;
    
    if(direction & UP)
        --target.row;
    
    if(direction & LEFT)
        --target.col;
    
    if(direction & DOWN)
        ++target.row;
    
    if(direction & RIGHT)
        ++target.col;
    
    if(target.row < 0)
        target.row = room.height - 1;
    
    if(target.col < 0)
        target.col = room.width - 1;
    
    if((short)room.height - 1 < target.row)
        target.row = 0;
    
    if((short)room.width - 1 < target.col)
        target.col = 0;
    
    if(target.col == player.pos.col && target.row == player.pos.row) //direction invalide
        return false;
    
    getByPosition(room.map, player.pos) = player.pid + KMaxPlayers; //ajoute un traceur    
    unsigned &TElem = getByPosition(room.map, target);
    
    
    if(TElem > KMaxPlayers){ //traceur
        GameRoom::killPlayer(room, player);
        Log::report("Player " + std::to_string(player.pid) + " kill by a tracer (" + std::to_string(TElem) + ")");
    }else{
        if(TElem != 0){
            GameRoom::killPlayer(room, *room.players[TElem - 1]);
            Log::report("Player " + std::to_string(TElem) + " kill by player " + std::to_string(player.pid));
        }
        TElem = player.pid;
    }
    
    player.pos = target;
    
    return true;
}

void GameRoom::killPlayer(SGameRoom& room, SPlayer& player){
    if(!isFull(room))
        return;
    
    player.alive = false;
    ++room.death;
    Packet::sendDEAD(room, player);
}

void GameRoom::removeGameRoom(SGameRoom& room){
    room.accessible = false;;
    room.curPlayers = 0;
    room.maxPlayers = 0;
    room.death = 0;
    room.height = 0;
    room.width = 0;
    room.id = 0;
    room.init = false;
    room.map.clear();
    room.playerTurn = 0;
    room.turnCount = 0;
    room.startTime = 0;
    
    for(SPlayer *player : room.players){
        if(player->client != nullptr)
            player->client->player = nullptr;
        
        delete player;
        player = nullptr;
    }
    
    room.players.clear();
}

void GameRoom::playerLogout(SPlayer& player){
    player.client = nullptr;
    SGameRoom &room = *player.gameRoom;
    
    if(player.gameRoom->accessible){ //the game is not started
        GameRoom::removePlayer(&player);
        Packet::sendToGameRoom(room, Packet::generateINFO(room));
    }else{ //in game
        player.client = nullptr;
        GameRoom::killPlayer(room, player);
        
        if(room.playerTurn == player.pid)
            GameRoom::startTurn(room);
    }
}