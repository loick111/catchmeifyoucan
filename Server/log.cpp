/** 
 * @file   log.cpp
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:47
 * @brief 
 */

#include <iostream>
#include <cstdarg>
#include <vector>
#include <cerrno>
#include <cstring>
#include <semaphore.h>
#include "log.h"

using namespace std;
using namespace Log;

namespace{
    sem_t mutex;
    enum Effect{
        RESET   = 0,
        BLACK   = 30,
        RED     = 31,
        GREEN   = 32,
        YELLOW  = 33,
        BLUE    = 34,
        MAGENTA = 35,
        CYAN    = 36,
        WHITE   = 37,
        BOLD    = 1
    };
    
    void color(const vector<Effect> & effects) {
        cerr << "\033[";
        
        for(unsigned i = 0; i < effects.size(); ++i){
            cerr << effects[i];
            
            if(i < effects.size() - 1)
                cerr << ';';
        }
        
        cerr << 'm';
    }
    
    void color(){
        color({Effect::RESET});
    }
}

void Log::init(){
    sem_init(&mutex, 0, 1);
}

void Log::report(const string& msg, Level lvl/* = Level::INFO*/){
    sem_wait(&mutex);
    switch(lvl){
        case INFO:
            color({Effect::BLUE, Effect::BOLD});
            cerr << "[INFO] ";
            break;
        case GOOD:
            color({Effect::GREEN, Effect::BOLD});
            cerr << "[GOOD] ";
            break;
        case FAIL:
            color({Effect::RED, Effect::BOLD});
            cerr << "[FAIL] ";
            break;
    }
    
    color();
    
    if(lvl == Level::FAIL){
        cerr << msg << endl;
        color({Effect::RED});
        cerr << "> message: " << strerror(errno) << endl;
        color();
    }else
        cout << msg << endl;
    sem_post(&mutex);
}
