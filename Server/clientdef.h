/** 
 * @file   clientdef.h
 * @author Vincent Quatrevieux
 *
 * @date 30 décembre 2013, 14:51
 * @brief 
 */

#ifndef CLIENTDEF_H
#define	CLIENTDEF_H

namespace Client{
    typedef struct SClient SClient;
}

#endif	/* CLIENTDEF_H */

