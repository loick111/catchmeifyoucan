/** 
 * @file   utils.h
 * @author Vincent Quatrevieux
 *
 * @date 1 janvier 2014, 16:33
 * @brief utilitaires globaux
 */

#ifndef UTILS_H
#define	UTILS_H


/**
 * Utilitaires
 */
namespace Utils{
    /**
     * Naturel aléatoire entre min (inclu) et max (exclu)
     * @param min nombre minimum
     * @param max nombre maximum (exclu)
     * @return entier naturel aléatoire
     */
    unsigned random(unsigned min, unsigned max);
}

#endif	/* UTILS_H */

