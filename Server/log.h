/** 
 * @file   log.h
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:47
 * @brief log system
 */

#ifndef LOG_H
#define	LOG_H

#include <string>

namespace Log{    
    enum Level{
        INFO = 0,
        GOOD = 1,
        FAIL = 2
    };
    
    void report(const std::string & msg, Level lvl = Level::INFO);
    void init();
}

#endif	/* LOG_H */

