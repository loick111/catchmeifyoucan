/** 
 * @file   constants.h
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 19:40
 * @brief 
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include <vector>

typedef int SOCKET;
typedef unsigned char packet_data;
typedef std::vector<packet_data> CVPacketData;

const unsigned KPort = 5555;
const unsigned KMaxClients = 8;
const unsigned KBufferSize = 64;
const unsigned KMaxSendQueueSize = 256;
const unsigned KMaxGameRooms = 16;
const unsigned KMaxPlayers = 8;
const unsigned KMaxWidth = 30;
const unsigned KMaxHeight = 15;
const unsigned KMinWidth = 10;
const unsigned KMinHeight = 10;

#endif	/* CONSTANTS_H */

