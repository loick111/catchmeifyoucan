/** 
 * @file   packet.cpp
 * @author Vincent Quatrevieux
 *
 * @date 25 décembre 2013, 22:29
 * @brief 
 */

#include <cstring>
#include <list>
#include <ctime>
#include "packet.h"
#include "log.h"
#include "gameroom.h"

using namespace Packet;

namespace{
    std::string packetDataToString(const CVPacketData & data){
        std::string str;
        
        for(packet_data d : data)
            str += std::to_string(d) + ' ';
        
        return str;
    }
    
    packet_data compressRoomData(const GameRoom::SGameRoom & room){
        return (room.maxPlayers - 1) << 5 | room.curPlayers << 1 | room.accessible;
    }
}
    
SPacket Packet::generateINFO(const GameRoom::SGameRoom & room){
    CVPacketData data(1);
    data[0] = compressRoomData(room);
    return generate(INFO, true, data);
}

bool Packet::processHELLO(SPacket & packet, Client::SClient& client){
    return send(client, packet); //ping
}

bool Packet::processLIST(SPacket& packet, Client::SClient& client){
    std::list<GameRoom::SGameRoom> rooms = GameRoom::getGameRooms();
    
    packet.data.reserve(rooms.size() + 1);
    packet.data[0] |= rooms.size();
    
    for(const GameRoom::SGameRoom & room : rooms){
        packet.data.push_back(compressRoomData(room));
    }
    
    return send(client, packet);
}

bool Packet::processCREATE(SPacket& packet, Client::SClient& client){
    packet_data nbPlayers = getHeadData(packet);
    
    if(nbPlayers < 2 || nbPlayers > KMaxPlayers){
        return send(client, generate(CREATE, 0));
    }
    
    try{
        GameRoom::create(nbPlayers);
    }catch(...){
        return send(client, generate(CREATE, 0));
    }
    
    Log::report("New GameRoom with " + std::to_string(nbPlayers) + " players", Log::GOOD);
    
    return send(client, generate(CREATE, 1));
}

bool Packet::processJOIN(SPacket& packet, Client::SClient& client){ 
    try{
        unsigned id = getHeadData(packet);
        GameRoom::SGameRoom &room = GameRoom::get(id);
        GameRoom::addPlayer(client, room);

        if(!send(client, generate(JOIN, 1)))
            return false;

        SPacket infoPacket = generateINFO(room);
        sendToGameRoom(room, infoPacket);
        
        if(GameRoom::isFull(room))
            return sendINIT(room);
        return true;
    }catch(...){
        return send(client, generate(JOIN, 0));
    }
}

bool Packet::processINFO(SPacket& packet, Client::SClient& client){
    unsigned id = getHeadData(packet);
    
    try{
        if(!sendINFO(client, id))
            return false;
    }catch(...){
        return send(client, generate(INFO, 0));
    }
    
    return true;
}

bool Packet::processINIT(SPacket& packet, Client::SClient& client){
    if(client.player == nullptr || !bool(getHeadData(packet)))
        return false;
    
    client.player->ready = true;
    
    bool ready = true;
    GameRoom::SGameRoom &room = *client.player->gameRoom;
    
    if(!GameRoom::isFull(room))
        return false;
    
    for(unsigned i = 0; i < room.players.size() && (ready = room.players[i]->ready); ++i);
    
    if(ready){
        GameRoom::startTurn(room);
    }
    
    return true;
}

bool Packet::processTURN(SPacket& packet, Client::SClient& client){
    return false;
}

bool Packet::processMOVE(SPacket& packet, Client::SClient& client){
    if(client.player == nullptr 
            || !client.player->ready 
            || !client.player->alive
            || client.player->gameRoom->playerTurn != client.player->pid)
        return false;
    
    unsigned direction = getHeadData(packet);
    GameRoom::SGameRoom & room = *client.player->gameRoom;
    
    if(!GameRoom::movePlayer(room, direction)){
        return send(client, generate(MOVE, 0));
    }
    
    sendToGameRoom(room, packet);
    
    GameRoom::startTurn(room);
    return true;
}

bool Packet::sendINFO(Client::SClient & client, unsigned id){    
    return send(client, generateINFO(GameRoom::get(id)));
}

bool Packet::parse(const CVPacketData & data, Client::SClient& client){
    SPacket recv;
    packet_data id = data[0] >> 4;
    for(const SPacket & packet : KPackets){
        if(packet.id == id){
            recv = packet;
            break;
        }
    }
    
    if(recv.name.size() == 0){ //packet not found
        Log::report("Recv: " + packetDataToString(data) + "(" + std::to_string(id) + "-UNKNOWN)");
        return false;
    }
    
    recv.data = data;
    
    Log::report("Recv: " + packetDataToString(data) + " (" + recv.name + ")");
    
    return recv.process(recv, client);
}

bool Packet::send(Client::SClient& client, const SPacket& packet, bool async/* = true*/){
    CVPacketData data;
    
    if(packet.data.size() == 0){
        data.push_back(packet.id << 4);
    }else{
        data = packet.data;
    }
    
    if(async){
        if(!Client::asyncWrite(client, data))
            return false;
    }else if(!Client::write(client, data)){
        return false;
    }

    Log::report("Send: " + packetDataToString(data) + " (" + packet.name + ") / size: " + std::to_string(data.size()));
    return true;
}

SPacket Packet::generate(const SPacket& model, packet_data data, const CVPacketData& extra/* = CVPacketData()*/){
    SPacket packet = model;
    packet.data.clear();
    packet.data.reserve(extra.size() + 1);
    packet_data head = packet.id << 4;
    head |= data;
    packet.data.push_back(head);
    
    for(packet_data d : extra)
        packet.data.push_back(d);
    
    return packet;
}

packet_data Packet::getHeadData(const SPacket & packet){
    if(packet.data.size() < 1)
        return 0;
    return packet.data[0] & ~(packet.id << 4);
}

void Packet::sendToGameRoom(const GameRoom::SGameRoom& room, const SPacket& packet){
    for(GameRoom::SPlayer *player : room.players){
        if(player->client == nullptr)
            continue;
        
        send(*player->client, packet);
    }
}

bool Packet::sendINIT(GameRoom::SGameRoom& room){
    if(!room.init || room.curPlayers != room.maxPlayers)
        return false;
    
    std::vector<packet_data> data;
    data.reserve(room.players.size() * 2 + 2);
    
    data.push_back(room.width);
    data.push_back(room.height);
    
    for(GameRoom::SPlayer *player : room.players){
        data.push_back(player->pos.row);
        data.push_back(player->pos.col);
    }
    
    for(GameRoom::SPlayer *player : room.players)
        Packet::send(*player->client, Packet::generate(INIT, player->pid, data));
    
    return true;
}

void Packet::sendTURN(GameRoom::SGameRoom& room){
    SPacket packet = generate(TURN, room.playerTurn);
    Packet::sendToGameRoom(room, packet);
}

void Packet::sendDEAD(GameRoom::SGameRoom& room, GameRoom::SPlayer& player){
    SPacket packet = generate(DEAD, player.pid);
    sendToGameRoom(room, packet);
}

void Packet::sendEND(GameRoom::SGameRoom& room){
    CVPacketData extra(5);
    
    uint16_t duration = time(NULL) - room.startTime;
    unsigned dest = 0;
    std::memcpy(&extra[dest], &duration, 2);
    dest += 2;
    
    uint16_t turnCount = room.turnCount;
    std::memcpy(&extra[dest], &turnCount, 2);
    dest += 2;
    
    extra[dest] = room.maxPlayers;
    
    auto winner = room.players.begin();
    for(;winner != room.players.end() && !(*winner)->alive; ++winner);
    
    SPacket packet = generate(END, (*winner)->pid, extra);
    sendToGameRoom(room, packet);
}
