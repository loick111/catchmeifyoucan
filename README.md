Catch Me if you can
===========

## Made by Quatrevieux Vincent, Mahieux Loïck, Perrot Gaëtan and Maceri Thomas

This is a project that we had to make in one month. We were groups of 4 guys. The subject of this project is available here: http://infodoc.iut.univ-aix.fr/~casali/CPlusPlus/CatchMe.htm

### So, what about the game ?

It is a game based on the same logic as "Snake" but with a remake like "Tron". In fact, you chose to be a cursor on a map, and your aim is to eat the other one and be the last man standing. 

You can play alone, versus 3 differents AI, or with one, two or three friends. 
The online mod is available too...You can be 8 players on the same map, you only have to chose the online mod and read the instructions in the "How To". 

A lot of things can be changed by players, as their color, their cursor, even the size of the map. 

If you want to know more about the game, you only haveto read the "HowTo". 
> /!\ You need to play the game on a Unix System to full-enjoy this game /!\

Thanks for reading, playing, and enjoy !

Loick, Vincent,Gaetan, and Thomas.
