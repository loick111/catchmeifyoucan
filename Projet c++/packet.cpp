/** 
 * @file   packet.cpp
 * @author Vincent Quatrevieux
 *
 * @date 26 décembre 2013, 17:50
 * @brief Define functions of nsPacket namespace
 */

#include <stdexcept>
#include "packet.h"
#include "socket.h"
#include "utils.h"

using namespace nsPacket;

bool nsPacket::Corresponds(packet_data Head, Packets Comp){
    return Head >> 4 == (packet_data)Comp;
}//nsPacket::Corresponds()

bool nsPacket::Corresponds(packet_data* Data, Packets Comp){
    return nsPacket::Corresponds(Data[0], Comp);
}//nsPacket::Corresponds()

bool nsPacket::Corresponds(const CVPacketData& Data, Packets Comp){
    return nsPacket::Corresponds(Data[0], Comp);
}//nsPacket::Corresponds()

CVPacketData nsPacket::Generate(Packets Packet, packet_data Data/* = 0*/, const CVPacketData & Extra/* = CVnsPacketData()*/){
    packet_data Head = (packet_data)Packet << 4;
    Head |= Data;
    
    CVPacketData PacketData(1 + Extra.size());
    PacketData[0] = Head;
    
    for(unsigned i = 0; i < Extra.size(); ++i){
        PacketData[i + 1] = Extra[i];
    }
    
    return PacketData;
}//nsPacket::Generate()

CVPacketData nsPacket::AskList(){
    CVPacketData Data;
    if(!nsSocket::Write(Generate(LIST))){
        throw std::runtime_error("Cannot write on socket !");
    }
    
    if(!nsSocket::Read(Data, 1)){
        throw std::runtime_error("Cannot read on socket !");
    }
    
    unsigned Size = Data[0];
    Size &= ~(LIST << 4);
    
    Data.clear();
    
    if(Size > 0)
        nsSocket::Read(Data, Size);
    
    return Data;
}//nsPacket::AskList()

bool nsPacket::NewRoom(unsigned short NbPlayers){
    nsSocket::Write(nsPacket::Generate(CREATE, NbPlayers));
    
    CVPacketData Data;
    if(!nsSocket::Read(Data, 1) || !nsPacket::Corresponds(Data, CREATE)){
        return false;
    }
    
    return bool(Data[0] & ~(CREATE << 4));
}//nsPacket::NewRoom()

CVPacketData nsPacket::AskRoomInfo(unsigned id){
    if(!nsSocket::Write(Generate(INFO, packet_data(id)))){
        throw std::runtime_error("Cannot write on socket !");
    }
    
    CVPacketData Data;
    if(!nsSocket::Read(Data, 2)){
        throw std::runtime_error("Cannot read on socket !");
    }
    
    return Data;
}//nsPacket::AskRoomInfo()

bool nsPacket::Join(unsigned id){
    if(!nsSocket::Write(Generate(JOIN, (packet_data)id))){
        throw std::runtime_error("Cannot write on socket !");
    }
    
    CVPacketData Data;
    if(!nsSocket::Read(Data, 1)){
        throw std::runtime_error("Cannot read on socket !");
    }
    
    return bool(Data[0] & ~(JOIN << 4));
}//nsPacket::Join()

bool nsPacket::WaitPlayers(CVPacketData& Data){
    Data.clear();
    
    if(!nsSocket::Read(Data, 2)){
        throw std::runtime_error("Cannot read on socket !");
    }
    
    if(Corresponds(Data, INFO)){
        return true;
    }
    
    if(Corresponds(Data, INIT)){
        CVPacketData Extra;
        if(!nsSocket::Read(Extra, KMaxPlayers * 3)){
            throw std::runtime_error("Cannot read on socket !");
        }
        
        Data.reserve(Data.size() + Extra.size());
        
        for(const packet_data d : Extra){
            Data.push_back(d);
        }
        
        return false;
    }
    
    throw std::runtime_error("Bad response");
}//nsPacket::WaitPlayers()

void nsPacket::SendInit(){
    if(!nsSocket::Write(nsPacket::Generate(INIT, true)))
        throw std::runtime_error("Cannot write on socket !");
} //SendInit()

packet_data nsPacket::WaitGamePacket(unsigned& Data){
    CVPacketData Packet;
    
    if(!nsSocket::Read(Packet, 1))
        throw std::runtime_error("Cannot read socket !");
    
    if(Corresponds(Packet, TURN)){
        Data = Packet[0] & ~(TURN << 4);
        return TURN;
    }
    
    if(Corresponds(Packet, MOVE)){
        Data = Packet[0] & ~(MOVE << 4);
        return MOVE;
    }
    
    if(Corresponds(Packet, DEAD)){
        Data = Packet[0] & ~(DEAD << 4);
        return DEAD;
    }
    
    if(Corresponds(Packet, END)){
        Data = Packet[0] & ~(END << 4);
        return END;
    }
    
    throw std::runtime_error("Bad response");
} //WaitGamePacket()

void nsPacket::SendMove(unsigned direction){
    if(!nsSocket::Write(Generate(MOVE, direction)))
        throw std::runtime_error("Cannot write on socket !");
} //SendMove()

bool nsPacket::Ping(unsigned Size){
    CVPacketData Packet = Generate(HELLO, 0);
    Packet.reserve(Size);
    
    for(unsigned i = 0; i < Size - 1; ++i){
        Packet.push_back(nsUtils::Random(0, 255));
    }
    
    if(!nsSocket::Write(Packet))
        return false;
    
    CVPacketData Response;
    if(!nsSocket::Read(Response, Size) || Response != Packet)
        return false;
    
    return true;
} //Ping()

CVPacketData nsPacket::GetEndExtraData(){
    CVPacketData Extra;
    if(!nsSocket::Read(Extra, 5))
        throw std::runtime_error("Cannot read on socket !");
    
    return Extra;
}//GetEndExtraData()