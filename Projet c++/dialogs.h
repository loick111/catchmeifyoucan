/** 
 * @file   dialogs.h
 * @author Vincent Quatrevieux
 *
 * @date december, 21st, 2013, 12:37
 * @brief Prototype of nsDialogs functions
 */

#ifndef DIALOGS_H
#define	DIALOGS_H

#include "game.h"
#include "datamanager.h"

/**
 * Messages, menu... Management
 */
namespace nsDialogs{
    /**
     * Main Menu
     */
    void MainMenu();
    
    /**
     * New game creation menu
     */
    void NewGame();
    
    /**
     * Type of player configuration
     * @param Player Player configuration
     */
    void ConfigPlayer(nsGame::SPlayer & Player);
    
    /**
     * End of game statistics
     * @param Game nsGame Over
     * @param Time Time of the nsGame
     * @param Turns revolution
     */
    void EndGame(const nsGame::SGame & Game, unsigned Time, unsigned Turns);
    
    /**
     * Server connection and Main menu online
     */
    void StartOnline();
    
    /**
     * Make a new online game
     */
    void NewOnline();

    /**
     * Display the tutorial
     */
    void HowTo();

    /**
     * Display a socket error (disconnection)
     * @param Msg Message of error to display
     * @return true to reconnect, else false
     */
    bool SocketError(const std::string & Msg);
    
    /**
     * Display the room informations
     * @param id Room ID
     */
    void RoomInfo(unsigned id);
    
    /**
     * Get the ping
     */
    void Ping();
    
    /**
     * Display stats of the game
     * @param gs SGameStats struct
     */
    void OnlineGameStats(const nsDataManager::SGameStats & gs);
}//nsDialogs

#endif	/* DIALOGS_H */

