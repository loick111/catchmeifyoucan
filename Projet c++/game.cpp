/**
 * @file   game.cpp
 * @author Vincent Quatrevieux
 *
 * @date 19 décembre 2013, 16:48
 * @brief Define functions of nsGame namespace
 */

#include <ctime>
#include <unistd.h>
#include "game.h"
#include "constants.h"
#include "map.h"
#include "ui.h"
#include "dialogs.h"
#include "AI.h"
#include "config.h"
#include "packet.h"
#include "datamanager.h"
#include "utils.h"

using namespace nsMap;
using namespace nsGame;

void nsGame::Run(MapElem NbPlayers) {
    SGame Game;

    InitPlayers(Game, NbPlayers);

    Init(Game, nsConfig::MapWidth, nsConfig::MapHeight);
    
    const unsigned StartTime = time(NULL);
    unsigned TurnCount = 0;
    
    nsUI::ShowMap(Game.Map);

    while (CanPlay(Game)) {
        for (SPlayer & Player : Game.Players) {
            if (!Player.Canplay) //si il ne peux pas jouer, on passe son tour
                continue;

            StartTurn(Game, Player);
            
            if(!CanPlay(Game))
                break;
        }
        ++TurnCount;
    }
    cout << "Partie terminée !" << endl;
    cout << "Appuyez sur une touche pour continuer ...";
    unsigned Duration = time(NULL) - StartTime;
    nsUI::KeyDown();
    
    nsDialogs::EndGame(Game, Duration, TurnCount);
    
}//nsGame::Run()

void nsGame::RunOnline(SGame & Game, const unsigned MyPid){    
    unsigned CurPlayer = 1;
    bool running = true;
    
    nsDataManager::SGameStats gs;
    bool ImDead = false;
    
    while(running){
        nsUI::ShowMap(Game.Map);
        cout << "Vous êtes le ";
        nsUI::Display("joueur " + std::to_string(MyPid) + " (" + nsConfig::PlayersToken[MyPid] + ")", nsConfig::PlayersColor[MyPid]);
        cout << endl;
        if(ImDead){
            nsUI::Display("Vous êtes mort !", nsUI::RED);
            cout << endl;
        }
        unsigned Data;
    
        switch(nsPacket::WaitGamePacket(Data)){
            case nsPacket::TURN:
                CurPlayer = Data;
                nsUI::Display(MyPid == CurPlayer ? "À vous de jouer" : "Joueur " + std::to_string(CurPlayer) + " (" + nsConfig::PlayersToken[MyPid] + ")" + "...", nsUI::BLACK, nsConfig::PlayersColor[CurPlayer]);
                cout << endl;
                if(MyPid == Data){ //au client de jouer
                    unsigned Direction = nsGame::GetDirection();
                    nsPacket::SendMove(Direction);
                }
                break;
            case nsPacket::MOVE:
                if(0 == Data && MyPid == CurPlayer){
                    nsUI::Display("Direction invalide !", nsUI::RED);
                    cout << endl;
                }else{
                    SPlayer & Player = Game.Players[CurPlayer - 1];
                    nsMap::MoveToken(Game.Map, Player, Data);
                }
                break;
            case nsPacket::DEAD:
                if(MyPid == Data){
                    nsUI::Display("Vous êtes mort !", nsUI::RED);
                    ImDead = true;
                }else{
                    nsUI::Display("Joueur " + std::to_string(Data), nsConfig::PlayersColor[Data]);
                    nsUI::Display(" mort !", nsUI::RED);
                }
                cout << endl;
                Game.Players[Data - 1].Canplay = false;
                sleep(2);
                break;
            case nsPacket::END:{
                running = false;
                CVPacketData Extra = nsPacket::GetEndExtraData();
                gs = nsDataManager::ParseEndPacket(Extra, Data);
                nsUI::Display("Partie terminé !", nsUI::GREEN);
                cout << endl;
                nsUI::Display("Appuyez sur une touche pour continuer...", nsUI::YELLOW);
                nsUI::KeyDown();
            }break;
        }
    }
    nsDialogs::OnlineGameStats(gs);
} //RunOnline()

void nsGame::StartTurn(SGame& Game, SPlayer& Player) {
    string Str = "Joueur " + std::to_string(Player.Pid) + " a vous de jouer !" + " (" + nsConfig::PlayersToken[Player.Pid] + ")";
    nsUI::Display(Str, nsUI::BLACK, nsConfig::PlayersColor[Player.Pid]);
    
    unsigned Direction = 0;

    switch (Player.AILevel) {
        case 0: //human
            Direction = GetDirection();
            break;
        case 1:
            Direction = nsAI::Type1(Game, Player);
            break;
        case 2:
            Direction = nsAI::Type2(Game, Player);
            break;
        case 3:
            Direction = nsAI::Type3(Game, Player);
            break;
    }
    
    MapElem Pid = nsMap::MoveToken(Game.Map, Player, Direction);

    if (Pid != 0 && Pid <= KMaxPlayers) {
        Game.Players[Pid - 1].Canplay = false;
        ++Game.Death;
    }
}//nsGame::StartTurn()

unsigned nsGame::GetDirection() {
    unsigned Direction = 0;
    do {
        switch (nsUI::KeyDown()) {
            case 122: //z: up
            case 90: //Z: up
                Direction = UP;
                break;
            case 115: //s: down
            case 83: //S: down
            case 120: //x: down
            case 88: //X: down
                Direction = DOWN;
                break;
            case 113: //q: left
            case 81: //Q: left
                Direction = LEFT;
                break;
            case 68: //d: right
            case 100: //D: right
                Direction = RIGHT;
                break;
            case 119: //w: down left
            case 87: //W: down left
                Direction = DOWN | LEFT;
                break;
            case 99: //c: down right
            case 67: //C: down right
                Direction = DOWN | RIGHT;
                break;
            case 97: //a: up left
            case 65: //A: up left
                Direction = UP | LEFT;
                break;
            case 101: //e: up right
            case 69: //E: up right
                Direction = UP | RIGHT;
                break;
        }
    } while (Direction == 0);
    
    return Direction;
}//nsGame::GetDirection()

void nsGame::RemovePlayer(SGame& Game, MapElem Pid) {
    SPlayer & Player = Game.Players[Pid - 1];
    nsMap::GetByCPosition(Game.Map, Player.Pos) = 0;
    Player.Canplay = false;
}//nsGame::RemovePlayer()

void nsGame::InitPlayers(SGame & Game, unsigned short nb) {
    Game.Players.resize(nb);

    for (MapElem Pid = 1; Pid <= nb; ++Pid) {
        Game.Players[Pid - 1].Pid = Pid;
        nsDialogs::ConfigPlayer(Game.Players[Pid - 1]);
    }
}//nsGame::initPlayers()

bool nsGame::CanPlay(const SGame& Game){
    return Game.Players.size() - Game.Death > 1;
}//nsGame::CanPlay()
