/**
 * @file   config.h
 * @author Gaëtan Perrot
 *
 * @date Created on 7 janvier 2014, 15:12
 * @brief Define functions of nsConfig namespace
 */
#include <fstream>
#include <iostream>
#include "constants.h"
#include "utils.h"
#include "config.h"
#include "ui.h"

unsigned nsConfig::MapHeight              = 10;
unsigned nsConfig::MapWidth               = 10;
std::string nsConfig::OnlineHost          = "online.prepa-dev.fr";
unsigned nsConfig::OnlinePort             = 5555;
    
vector<char> nsConfig::PlayersToken = {
    KEmpty,
    'X',
    'O',
    '#',
    '@',
    '$',
    '*',
    '%',
    '+'
};

std::vector<nsUI::Effect> nsConfig::PlayersColor = {
    nsUI::RESET,
    nsUI::RED,
    nsUI::BLUE,
    nsUI::GREEN,
    nsUI::YELLOW,
    nsUI::RED,
    nsUI::MAGENTA,
    nsUI::CYAN,
    nsUI::WHITE
};

using namespace std;
using namespace nsConfig;

namespace{
    nsUI::Effect ParseColor(string StrColor){
        if(StrColor == "RED")
            return nsUI::Effect::RED;
        else if(StrColor == "GREEN")
            return nsUI::Effect::GREEN;
        else if(StrColor == "YELLOW")
            return nsUI::Effect::YELLOW;
        else if(StrColor == "BLUE")
            return nsUI::Effect::BLUE;
        else if(StrColor == "MAGENTA")
            return nsUI::Effect::MAGENTA;
        else if(StrColor == "CYAN")
            return nsUI::Effect::CYAN;
        else if(StrColor == "WHITE")
            return nsUI::Effect::WHITE;
        
        return nsUI::WHITE;
    }
}

void nsConfig::LoadConfig(){
    ifstream ifs;
    ifs.open (KConfigFile);  
    
    if(ifs.is_open()){
        for(string Name; ifs >> Name;){
            if(Name == "MapHeight:")
                ifs >> nsConfig::MapHeight;
            else if (Name == "MapWidth:")
                ifs >> nsConfig::MapWidth;
            else if(Name == "OnlineHost:")
                ifs >> nsConfig::OnlineHost;
            else if(Name == "OnlinePort:")
                ifs >> nsConfig::OnlinePort;
            else if(Name == "Player1:"){
                ifs >> nsConfig::PlayersToken[1];
                string str; ifs >> str;
                nsConfig::PlayersColor[1] = ParseColor(str);
            }else if(Name == "Player2:"){
                ifs >> nsConfig::PlayersToken[2];
                string str; ifs >> str;
                nsConfig::PlayersColor[2] = ParseColor(str);
            }else if(Name == "Player3:"){
                ifs >> nsConfig::PlayersToken[3];
                string str; ifs >> str;
                nsConfig::PlayersColor[3] = ParseColor(str);
            }else if(Name == "Player4:"){
                ifs >> nsConfig::PlayersToken[4];
                string str; ifs >> str;
                nsConfig::PlayersColor[4] = ParseColor(str);
            }else if(Name == "Player5:"){
                ifs >> nsConfig::PlayersToken[5];
                string str; ifs >> str;
                nsConfig::PlayersColor[5] = ParseColor(str);
            }else if(Name == "Player6:"){
                ifs >> nsConfig::PlayersToken[6];
                string str; ifs >> str;
                nsConfig::PlayersColor[6] = ParseColor(str);
            }else if(Name == "Player7:"){
                ifs >> nsConfig::PlayersToken[7];
                string str; ifs >> str;
                nsConfig::PlayersColor[7] = ParseColor(str);
            }else if(Name == "Player8:"){
                ifs >> nsConfig::PlayersToken[8];
                string str; ifs >> str;
                nsConfig::PlayersColor[8] = ParseColor(str);
            }
        }

        ifs.close();
    }
}//LoadConfig()