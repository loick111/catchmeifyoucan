/** 
 * @file   packet.h
 * @author Vincent Quatrevieux
 *
 * @date december, 23th, 2013, 17:50
 * @brief Packets list and function prototypes of nsPacket namespace
 */

#ifndef PACKET_H
#define	PACKET_H

#include "constants.h"

/**
 * Packets management 
 */
namespace nsPacket{
    /**
     * Available packets list
     */
    enum Packets{
        /**
         * packet received on connexion.
         * it's also use as "ping" packet
         * @note size 1 to 64
         */
        HELLO  = 1,
        /**
         * packet use for ask and receive the list of game rooms.
         * You don't have to put data for ask list.
         * The head data is the size of extra data (in bytes), therefore theire is a maximum of 16 game rooms (1 byte by room).
         * The extra data is the same has INFO packet
         * @see nsPacket::Packets::INFO
         * @note size 1 to 17
         */
        LIST   = 2,
        /**
         * packet use for create a new game room.
         * The head data is the size of the room (number of players).
         * The server returns a boolean (true if the room is created)
         * @note size 1
         */
        CREATE = 3,
        /**
         * packet use for join a game room.
         * The head data is the id of the room (0 to 15).
         * The server responds a boolean
         * @note size 1
         */
        JOIN   = 4,
        /**
         * Packet use to get the information of a single room (contrary to LIST).
         * The head data is use only for ask. It contains the room id.
         * The extra data is on use for server response.
         * The first bit is a boolean, it tell if the room is accessible.
         * The 4 bits after are an unsigned, the curent number of players.
         * The 3 last bits are an unsigned, the maximum of player minus 1 (8 players limit).
         * @note size 1 for ask or 2 for response
         */
        INFO   = 5,
        /**
         * Packet send by the server for ask that the game room is ready.
         * Theire isn't an head data in the INIT server packet.
         * The extra data is compozed by : <br>
         * - width of map in first byte <br>
         * - height for in byte <br>
         * - line of the first player <br>
         * - column of the first player <br>
         * - line of the second player <br>
         * ...<br>
         * For the client response, theire is only an head data, a boolean.
         * It send after the local copy of the game (map, players...), with true (success)
         * @note size 1 for client, 3 + 2 * number of players (7 to 19)
         */
        INIT   = 6,
        /**
         * Packet send by the server during a game to say who is the curent player.
         * The head data is the player's pid.
         * @warning The client (this program) shouldn't send this packet !
         * @note size 1
         */
        TURN   = 7,
        /**
         * Packet send by the client during a game to tell the moving direction.
         * The head data is the direction (same as local).
         * The server responds the same packet (same data) to all
         * if the direction is correct 
         * else head data = 0 to the curent player is the direction is invalid
         * @note size 1
         */
        MOVE   = 8,
        /**
         * Packet send by the server to tell the death of a player 
         * (not in real life, this isn't Mme Irma, or everybody else !).
         * The head data contains the pid of death player (god rest her soul).
         * @warning the client shouldn't send this packet, you can't kill someone like that
         * @note size 1
         */
        DEAD   = 9,
        /**
         * This is the end of the game, everyone are death :'( [expect me :)].
         * The head data contains the pid of the winner.
         * The extra data contains : <br/>
         * - The duration of the game (in seconds) : int16_t, 2 bytes <br>
         * - The number of turns : int16_t, 2 bytes <br/>
         * - The number of players, 1 byte
         * @warning The client shouldn't send the packet !
         * @note size 6
         */
        END    = 10
    };
    
    /**
     * test If a packet header does match to a packet
     * @param Head Packet header
     * @param Comp Packet to be comparated with
     * @return true If it matches. Else, false.
     */
    bool Corresponds(packet_data Head, Packets Comp);
    
    /**
     * test If a packet header does match to a packet
     * @param Data Packets data
     * @param Comp Packets to be comparated with
     * @return true if it does match, else false
     */
    bool Corresponds(packet_data *Data, Packets Comp);
    
    /**
     * test If a packet header does match to a packet
     * @param Data Packet data
     * @param Comp Packet to be comparated with
     * @return true if it does match, else false
     */
    bool Corresponds(const CVPacketData & Data, Packets Comp);
    
    /**
     * Generates the data (binaries) of a packet
     * @param Packet Packet to be sent
     * @param Data Header data
     * @param Extra Additionnal data
     * @return Packet data
     */
    CVPacketData Generate(Packets Packet, packet_data Data = 0, const CVPacketData & Extra = CVPacketData());
    
    /**
     * Ask the list of the Game Rooms (packet LIST)
     * @return Packet data
     */
    CVPacketData AskList();
    
    /**
     * Ask the server to make a new Game Room
     * @param NbPlayers Number of players
     * @return true if it does work, else false
     */
    bool NewRoom(unsigned short NbPlayers);
    
    /**
     * Ask the game room informations
     * @param id Game Room ID
     * @return Compressed data
     */
    CVPacketData AskRoomInfo(unsigned id);
    
    /**
     * Rejoin une game room
     * @param id id de la game room
     * @return true on success, false on error
     */
    bool Join(unsigned id);
    
    /**
     * Wait for another playerinto the game room
     * @param Data Game room compressed informations if the function send true, else, map data
     * @return true while there is still some places, false when the game is initialized
     */
    bool WaitPlayers(CVPacketData & Data);
    
    /**
     * Say to the server that everything's ready
     */
    void SendInit();
    
    /**
     * Wait for a game packet (TURN, MOVE, DEAD, END)
     * @param Data Packet data (all)
     * @return Received data ID
     */
    packet_data WaitGamePacket(unsigned &Data);
    
    /**
     * Send a movement packet
     * @param Direction Taken direction
     */
    void SendMove(unsigned Direction);
    
    /**
     * Send a packet HELLO to test the connection
     * @param Size Packet size(octets)
     * @return true on success, false on faillure
     */
    bool Ping(unsigned Size);
    
    /**
     * Get the extra data of the END Packet
     * @return The extra data
     */
    CVPacketData GetEndExtraData();
}//nsPacket

#endif	/* PACKET_H */

