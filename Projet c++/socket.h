/** 
 * @file   socket.h
 * @author Vincent Quatrevieux
 *
 * @date december 26th 2013, 16:36
 * @brief nsSocket namespace function prototype
 */

#ifndef SOCKET_H
#define	SOCKET_H

#include "constants.h"

/**
 * Management of the communication with the server 
 */
namespace nsSocket{
    /**
     * Socket representation
     */
    typedef int SOCKET;
    
    /**
     * Initialize the connexion
     * @return false if error, true if connected
     */
    bool Init();
    
    /**
     * Write a Packet to the server
     * @param Data Packet to write
     * @return true on success, false on faillure
     */
    bool Write(const CVPacketData & Data);
    
    /**
     * Read a packet sent from the server
     * @param Data packet to receive
     * @param Size Size to read
     * @return true on success, false on failure
     */
    bool Read(CVPacketData & Data, unsigned Size);
    
    /**
     * Close the connection with the server
     */
    void Close();
}//nsSocket

#endif	/* SOCKET_H */

