/** 
 * @file   utils.cpp
 * @author Vincent Quatrevieux
 *
 * @date 20 décembre 2013, 18:08
 * @brief Define functions of nsUtils namespace
 */

#include <ctime>
#include <sys/time.h>
#include <cstdlib>
#include "utils.h"

namespace{ //Private
    bool Is_Init = false;
}

unsigned nsUtils::Random(unsigned Min, unsigned Max){
    if(!Is_Init){
        srand(time(NULL));
        Is_Init = true;
    }
    
    return (rand() % (Max - Min)) + Min;
}//nsUtils::Random()

unsigned nsUtils::MaxLength(const std::vector<std::string>& V){
    unsigned Length = 0;
    
    for(const std::string & Str : V){
        if(Str.size() > Length)
            Length = Str.size();
    }
    
    return Length;
}//nsUtils::MaxLength()

unsigned long nsUtils::Microtime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    
    return (unsigned long)(tv.tv_sec * 1000000) + tv.tv_usec;
} //Microtime()