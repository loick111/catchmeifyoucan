/**
 * @file   game.h
 * @author Vincent Quatrevieux
 *
 * @date december, 19th, 2013, 16:48
 * @brief Prototype and functions of the namespace nsGame
 */

#ifndef GAME_H
#define	GAME_H

#include "constants.h"
#include "ui.h"

/**
 * nsGame management
 */
namespace nsGame {

    /**
     * Player struct
     */
    typedef struct {
        /** Position */
        CPosition Pos;
        /** Player ID */
        MapElem Pid;
        /** If the player is able to play */
        bool Canplay = true;
        /**
         * AI level.
         * 0 = human, 1 = AI with random moves
         */
        unsigned short AILevel = 0;
    } SPlayer;

    /**
     * Game structure
     */
    typedef struct {
        /** Game map */
        CMap Map;
        /** Players list */
        std::vector<SPlayer> Players;
        /** Dead players counter */
        MapElem Death = 0;
    } SGame;

    /**
     * Launch a game
     */
    void Run(MapElem NbPlayers);
    
    /**
     * Launch an online game
     * @param Game the copy of the inline game
     * @param MyPid The client's pid
     */
    void RunOnline(SGame & Game, const unsigned MyPid);
    
    /**
     * Remove a player
     * @param Game The game
     * @param Player Current player
     */
    void RemovePlayer(SGame & Game, MapElem Player);

    /**
     * Initialize players parameters
     * @param Game The game
     * @param nb Number of players
     */
    void InitPlayers(SGame & Game, unsigned short nb);
    
    /**
     * Beginning of a player turn
     * @param Game The game
     * @param Player Current player
     */
    void StartTurn(SGame & Game, SPlayer & Player);
    
    /**
     * Keyboard's keys management and give the right direction
     * @return The right direction
     */
    unsigned GetDirection();
    
    /**
     * goodness-of-game the game
     * @param Game Game to be tested
     * @return true if we're able to play, false if the game is over
     */
    bool CanPlay(const SGame & Game);
}//nsGame

#endif	/* GAME_H */
