/** 
 * @file   map.cpp
 * @author Vincent Quatrevieux
 *
 * @date 19 décembre 2013, 14:44
 * @brief Define functions of nsMap namespace
 */

#include "map.h"
#include "constants.h"
#include "ui.h"
#include "utils.h"

void nsMap::Init(nsGame::SGame & Game, unsigned Width, unsigned Height){
    Game.Map = nsMap::GenerateBlankMap(Width, Height);

    for(nsGame::SPlayer & Player : Game.Players){
        for(;;){
            Player.Pos.first = nsUtils::Random(0, Height);
            Player.Pos.second = nsUtils::Random(0, Width);
            
            MapElem & PElem = nsMap::GetByCPosition(Game.Map, Player.Pos);
            
            if(PElem == 0){
                PElem = Player.Pid;
                break;
            }
        }
    }
}//nsMap::Init()

CMap nsMap::GenerateBlankMap(unsigned Width, unsigned Height){
    CMap Map(Height);
    
    for(CVLine & Line : Map)
        Line.resize(Width);
    
    return Map;
} //GenerateBlankMap()

MapElem nsMap::MoveToken(CMap & Map, nsGame::SPlayer& Player, unsigned Direction){
    CPosition Target = nsMap::ParseDirection(Map, Player.Pos, Direction);
    
    MapElem & PElem = nsMap::GetByCPosition(Map, Player.Pos);
     
    PElem = Player.Pid + KMaxPlayers;
    
    MapElem & TElem = nsMap::GetByCPosition(Map, Target);
    MapElem Ret;
    
    Player.Pos = Target;
    
    if(TElem != 0 && TElem > KMaxPlayers){ //si traceur
        Ret = Player.Pid; //joueur mort
    }else{
        Ret = TElem;
        TElem = Player.Pid; //sinon on remplace l'ancienne case par le joueur 
    }
    
    nsUI::ShowMap(Map);
    
    return Ret;
}//nsMap::MoveToken()

MapElem & nsMap::GetByCPosition(CMap& Map, const CPosition& Pos){
    return Map[Pos.first][Pos.second];
}//nsMap::GetByCPosition()

CPosition nsMap::ParseDirection(const CMap & Map, const CPosition& Start, unsigned Direction){
    CPosition Target = Start;
    
    if(Direction & nsMap::Direction::UP)
        --Target.first;
    
    if(Direction & nsMap::Direction::LEFT)
        --Target.second;
    
    if(Direction & nsMap::Direction::DOWN)
        ++Target.first;
    
    if(Direction & nsMap::Direction::RIGHT)
        ++Target.second;
    
    if(Target.first < 0)
        Target.first = Map.size() - 1;
    
    if(Target.second < 0)
        Target.second = Map[Target.first].size() - 1;
    
    if((int)Map.size() - 1 < Target.first)
        Target.first = 0;
    
    if((int)Map[Target.first].size() - 1 < Target.second)
        Target.second = 0;
    
    return Target;
}//nsMap::ParseDirection()