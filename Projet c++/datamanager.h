/** 
 * @file   datamanager.h
 * @author Vincent Quatrevieux
 *
 * @date december, 26, 2013, 18:54
 * @brief Datamanage type, and prototype
 */

#ifndef DATAMANAGER_H
#define	DATAMANAGER_H

#include <vector>
#include "constants.h"
#include "game.h"

/**
 * Online data management
 */
namespace nsDataManager{
    /**
     * Game room data
     */
    typedef struct{
        /** The game room id*/
        unsigned id;
        /** maximum number of players*/
        unsigned MaxPlayers;
        /** curent number of players*/
        unsigned CurPlayers;
        /** room is accessible*/
        bool Accessible;
    } SGameRoom;
    
    /**
     * Store the online game stats
     */
    typedef struct{
        /** Number of players*/
        unsigned NbPlayers;
        /** duration of game*/
        unsigned Time;
        /** Player id of winner*/
        unsigned Winner;
        /** number of turns*/
        unsigned TurnCount;
    } SGameStats;
    
    /**
     * Return game room list
     * @return All game rooms
     */
    std::vector<SGameRoom> GetGameRooms();
    
    /**
     * Return game room informations
     * @param id nsGame room ID
     * @return Data
     */
    SGameRoom GetRoomInfo(unsigned id);
    
    /**
     * Analyse and decompress data from an info package to a game room
     * @param Data Part of the package which give informations
     * @param id nsGame room ID
     * @return Data 
     */
    SGameRoom ParseInfoPacket(packet_data Data, unsigned id);
    
    /**
     * Parse the INIT packet and create the game
     * @param Data The INIT packet
     * @param MyPid the client pid (out param)
     * @return the generated game
     */
    nsGame::SGame ParseInitPacket(const CVPacketData & Data, unsigned & MyPid);
    
    /**
     * Parse the END packet and return the game stats
     * @param Extra The extradata of END packet
     * @param Winner The winner pid (the head data in END packet)
     * @return The stats struct
     */
    SGameStats ParseEndPacket(const CVPacketData & Extra, const unsigned Winner);
}//nsDataManager

#endif	/* DATAMANAGER_H */

