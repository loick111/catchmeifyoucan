/**
 * @file   ui.h
 * @author Vincent Quatrevieux
 *
 * @date 19 décembre 2013, 13:08
 * @brief Functions Prototype, constants of the namespace nsUI
 */

#ifndef nsUI_H
#define	nsUI_H

#include <vector>
#include <string>
#include "constants.h"

/**
 * Display and keys management
 */
namespace nsUI {
    /**
     * Effects on the terminal (colors)
     */
    enum Effect{
        RESET   = 0,
        BLACK   = 30,
        RED     = 31,
        GREEN   = 32,
        YELLOW  = 33,
        BLUE    = 34,
        MAGENTA = 35,
        CYAN    = 36,
        WHITE   = 37,
        BOLD    = 1
    };
    
//    /**
//     * Players tokens list
//     */
//    const std::vector<char> KTokens = {
//        KEmpty,
//        nsConfig::TokenPlayer1,
//        nsConfig::TokenPlayer2,
//        nsConfig::TokenPlayer3,
//        nsConfig::TokenPlayer4,
//        nsConfig::TokenPlayer5,
//        nsConfig::TokenPlayer6,
//        nsConfig::TokenPlayer7,
//        nsConfig::TokenPlayer8
//    };
//    
//    /**
//     * Players colors list
//     */
//    const std::vector<Effect> KTokensEffect = {
//        RESET,
//        RED,
//        BLUE,
//        GREEN,
//        YELLOW,
//        RED,
//        MAGENTA,
//        CYAN,
//        WHITE
//    };

    /**
     * Display an element in the terminal, with the color
     * @param Disp Element to display
     * @param Color Text color, constant of nsUI::Effect
     * @param Background Background color, constant nsUI::Effect
     */
    template<typename TDisp>
    void Display(const TDisp & Disp, Effect Color, Effect Background = RESET);
    
    /**
     * Erase the termina
     */
    void Clear();
    
    /**
     * Display the Game map
     * @param Map Game map
     */
    void ShowMap(const CMap & Map);
    
    /**
     * Take the key, 
     * inspired of : http://www.daniweb.com/software-development/cpp/threads/11811/replacement-of-getch#post704917
     * @return The key ID
     */
    unsigned KeyDown();
    
    /**
     * Generate a menu, and return the choice
     * @param Msg Message lines to display, withthe title in 0
     * @param Menu Options list
     * @return Chosen option, begin from 1
     */    
    unsigned Menu(const std::vector<std::string> & Msg, const std::vector<std::string> & Menu);
    
    /**
     * Generate a menu, and return the choice
     * @param Prompt Message (or question) to display
     * @param Menu Option list
     * @return Chosen option, begin from 1
     */
    unsigned Menu(const std::string & Prompt, const std::vector<std::string> & Menu);
    
    /**
     * Generate the borders, top or botom
     * @param Size Box size
     */
    void BorderBox(unsigned Size);
    
    /**
     * Display a line in the box
     * @param Disp Line to display
     * @param Boxsize Box size
     * @param Color Text color
     * @param Background Text background
     * @param Center true to center, false to align at left
     */
    void BoxLine(const std::string & Disp, unsigned Boxsize, Effect Color = Effect::RESET, Effect Background = Effect::RESET, bool Center = false);
    
    /**
     * generate a table
     * @param Table Matrix of the table
     * @param Caption Table tite
     */
    void Table(const std::vector<std::vector<std::string> > & Table, const std::string & Caption);
    
    /**
     * Border top and bottom of the table
     * @param ColSizes Column size
     */
    void BorderTable(const std::vector<unsigned> & ColSizes);
}//nsUI

#include "ui.hpp"

#endif	/* nsUI_H */

