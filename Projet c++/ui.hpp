/**
 * @file   ui.hpp
 * @author Vincent Quatrevieux
 *
 * @date december 19th 2013, 14:32
 * @brief nsUI namespace functions content with template
 */

#ifndef nsUI_HPP
#define	nsUI_HPP

#include <iostream>

#include "ui.h"

using namespace std;

template<typename TDisp>
void nsUI::Display(const TDisp & Disp, nsUI::Effect Color, nsUI::Effect Background/* = 0*/) {
    cout << "\033[" << Color;

    if (Background != nsUI::Effect::RESET)
        cout << ';' << Background + 10;

    cout << 'm' << Disp << "\033[0m";
}//nsUI::Display()

#endif	/* nsUI_HPP */

