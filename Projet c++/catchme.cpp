/**
 * @file   catchme.cpp
 * @author Vincent Quatrevieux
 *
 * @date 19 décembre 2013, 13:07
 * @brief Start file
 */

/**
 * @mainpage Catch me if you can !
 * @author Loïck Mahieux, Thomas Maceri, Gaëtan Perrot, Vincent Quatrevieux
 * @see http://github.com/vincent4vx/Cpp-project
 *
 * @note This is a project that we had to make in one month. We were groups of 4 guys. The subject of this project is available here: http://infodoc.iut.univ-aix.fr/~casali/CPlusPlus/CatchMe.htm
 *
 * @section about So, what about the game ?
 *
 * It is a game based on the same logic as "Snake" but with a remake like "Tron". In fact, you chose to be a cursor on a map, and your aim is to eat the other one and be the last man standing. 
 *
 * You can play alone, versus 7 differents AI, or friends.
 * The online mod is available too...You can be 8 players on the same map, you only have to chose the online mod and read the instructions in the "How To". 
 *
 * A lot of things can be changed by players, as their color, their cursor, even the size of the map. 
 *
 * If you want to know more about the game, you only haveto read the "HowTo". 
 * @warning You need to play the game on a Unix System to full-enjoy this game.
 *
 * Thanks for reading, playing, and enjoy !
 *
 * Loïck, Vincent, Gaëtan, and Thomas.
 */

#include "dialogs.h"
#include "tests.h"
#include "config.h"

using namespace std;

/**
 * Start function
 * @param argc args count
 * @param argv args value
 * @return 0 on success
 */
int main(int argc, char* argv[]){
    if(argc > 1){
        if(string(argv[1]) == "test")
            nsTests::StartTests();
        else if(string(argv[1]) == "ai3"){
            nsTests::TestAIType3();
            return 0;
        }
    }
    
    nsConfig::LoadConfig();
    nsDialogs::MainMenu();
    return 0;
}//main()
