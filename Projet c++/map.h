/** 
 * @file   map.h
 * @author Vincent Quatrevieux
 *
 * @date december, 19th, 2013, 14:44
 * @brief Namespace nsMap Prototypes and functions 
 */

#ifndef MAP_H
#define	MAP_H

#include "constants.h"
#include "game.h"

/**
 * Map management
 */
namespace nsMap{
    /**
     * Main directions
     */
    enum Direction{
        UP = 1,
        LEFT = 2,
        DOWN = 4,
        RIGHT = 8
    };
    
    /**
     * Initialize the map of the game
     * @param Game The nsGame
     * @param Width Width of the map
     * @param Height Height of the map
     */
    void Init(nsGame::SGame & Game, unsigned Width, unsigned Height);
    
    /**
     * Generate a map without players
     * @param Width Map Width
     * @param Height Map Height
     * @return The blank map
     */
    CMap GenerateBlankMap(unsigned Width, unsigned Height);
    
    /**
     * Player's move management on the map
     * @param Map Map
     * @param Player The player to be moved
     * @param Direction Chosen direction, thanks to the enum Map::Direction
     * @return The target element
     */
    MapElem MoveToken(CMap & Map, nsGame::SPlayer & Player, unsigned Direction);
    
    /**
     * Return the element to the right position
     * @param Map Map
     * @param Pos The wanted position
     * @return The wanted element
     */
    MapElem & GetByCPosition(CMap & Map, const CPosition & Pos);
    
    /**
     * Analyse the direction according to the map, and the position to return
     * @param Map Map
     * @param Start Position of the beginning
     * @param Direction Wanted Direction
     * @return Wanted position
     */
    CPosition ParseDirection(const CMap & Map, const CPosition & Start, unsigned Direction);
}//nsMap

#endif	/* MAP_H */

