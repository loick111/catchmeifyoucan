/**
 * @file   constants.h
 * @author Vincent Quatrevieux
 *
 * @date december, 19, 2013, 14:02
 * @brief Types and List of Constents
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include <vector>
#include <string>

//typdedef
/** Type of the map case */
typedef unsigned short          MapElem;
/** Map Line */
typedef std::vector<MapElem>    CVLine;
/** Map type */
typedef std::vector<CVLine>     CMap;
/** Player Position */
typedef std::pair<short, short> CPosition;
/** Type of packet element */
typedef unsigned char packet_data;
/** Type of packet data*/
typedef std::vector<packet_data> CVPacketData;
//END typedef

//global constants
/** Empty character */
const char KEmpty               = ' ';
/** Maximum number of player */
const unsigned char KMaxPlayers = 8;
/** Configuration file way */
const std::string KConfigFile   = "config";
/** Max number of online game rooms*/
const unsigned KMaxOnlineRooms  = 16;
//END global constants

#endif	/* CONSTANTS_H */
