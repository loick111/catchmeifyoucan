/**
 * @file   utils.h
 * @author Vincent Quatrevieux
 *
 * @date december 20th 2013, 18:09
 * @brief Prototype of the namespace nsUtils
 */

#ifndef UTILS_H
#define	UTILS_H

#include <string>
#include <vector>

/**
 * Utility
 */
namespace nsUtils{
    /**
     * Random natural number between min (inclu) and max (exclu)
     * @param Min Minimum number
     * @param Max Maximum Number
     * @return Random Int
     */
    unsigned Random(unsigned Min, unsigned Max);
    
    /**
     * Size of the lagest string of the vector
     * @param V String vector
     * @return Largest size
     */
    unsigned MaxLength(const std::vector<std::string> & V);
    
    /**
     * Random rmake of a table
     * @param V Vector to be sorted
     */
    template<typename T>
    void Shuffle(std::vector<T> & V);
    
    /**
     * Get the current Unix timestamp in microseconds
     * @return Unix timestamp in microseconds
     */
    unsigned long Microtime();

    /**
     * Push all values of a vector in another vector
     * @param Vector The final vecotr
     * @param Values Values to push
     */
    template<typename T>
    void PushAll(std::vector<T> & Vector, const std::vector<T> & Values);
}//nsUtils

#include "utils.hpp"

#endif	/* UTILS_H */

