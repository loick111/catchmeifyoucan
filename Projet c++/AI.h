/** 
 * @file   AI.h
 * @author Vincent Quatrevieux
 *
 * @date december, 21st, 2013, 14:28
 * @brief nsAI namespace's functions prototypes and content
 */

#ifndef AI_H
#define	AI_H

#include "map.h"
#include "game.h"


using namespace nsMap;
using namespace nsGame;

/**
 * Artificial intelligence management
 */
namespace nsAI{
    /** Direction list */
    const std::vector<unsigned> KDirections = {
        UP,
        LEFT,
        DOWN,
        RIGHT,
        UP | LEFT,
        UP | RIGHT,
        DOWN | LEFT,
        DOWN | RIGHT
    };
    
    /**
     * Completely random AI
     * @param Game Game object
     * @param Player Player
     * @return Chosen direction
     */
    unsigned Type1(SGame & Game, SPlayer & Player);
    
    /**
     * Proximity choice, direct or random
     * @param Game Game object
     * @param Player Player
     * @return Direction found
     */
    unsigned Type2(SGame & Game, SPlayer & Player);
    
    /**
     * Nearest choice
     * @param Game Game object
     * @param Player Player
     * @return Direction found
     */
    unsigned Type3(SGame & Game, SPlayer & Player);
}//nsAI

#endif	/* AI_H */

