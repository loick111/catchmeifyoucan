/** 
 * @file   AI.cpp
 * @author Vincent Quatrevieux
 *
 * @date 21 décembre 2013, 14:28
 * @brief Definitions of AIs
 */

#include <math.h>

#include "AI.h"
#include "utils.h"

using namespace nsAI;

namespace{ 
    /**
     * Get the shortest distance between 2 players
     * @param Player1 The first player
     * @param Player2 the second player
     * @param Width Map width
     * @param Height map height
     * @return The distance
     */
    double DistPlayers(const CPosition& Player1, const CPosition& Player2, const unsigned Width, const unsigned Height){
        vector<CPosition> P2TorusPos = {
            Player2,
            CPosition(Player2.first - Height, Player2.second),
            CPosition(Player2.first + Height, Player2.second),
            CPosition(Player2.first, Player2.second - Width),
            CPosition(Player2.first, Player2.second + Width),
            CPosition(Player2.first - Height, Player2.second + Width),
            CPosition(Player2.first + Height, Player2.second - Width),
            CPosition(Player2.first + Height, Player2.second + Width),
            CPosition(Player2.first - Height, Player2.second - Width)
        };
        double MinDist = 10000000000;
        for(const CPosition & Pos : P2TorusPos){
            double Dist = sqrt((Player1.first - Pos.first) * (Player1.first - Pos.first) + (Player1.second - Pos.second) * (Player1.second - Pos.second));
            if(Dist < MinDist)
                MinDist = Dist;
        }
        return MinDist;
    }//DistPlayers()
    
    /**
     * Get the shortest distance between 2 players
     * @param Player1 The first player position
     * @param Player2 the second player position
     * @param Width Map width
     * @param Height map height
     * @return The distance
     */    
    double DistPlayers(const SPlayer & Player1, const SPlayer & Player2, const unsigned Width, const unsigned Height){
        return DistPlayers(Player1.Pos, Player2.Pos, Width, Height);
    } //DistPlayers()
    
    /**
     * Get the nearest player
     * @param Game The curent game
     * @param Player The player
     * @return The nearest player
     */
    SPlayer NearestPlayer(SGame& Game, const SPlayer& Player){
        const unsigned Height = Game.Map.size();
        const unsigned Width = Game.Map[0].size();
        //init nearest
        SPlayer Nearest;
        /*if(Game.Players[0].Pid == Player.Pid)
            Nearest = Game.Players[1];
        else
            Nearest = Game.Players[0];*/
        unsigned i = 0;
        do{
            Nearest = Game.Players[i++];
        }while(i < Game.Players.size() && Nearest.Pid == Player.Pid && !Nearest.Canplay);
        
        //cherche plus pres
        for(SPlayer Pers : Game.Players){
            if(Pers.Pid != Player.Pid && Pers.Canplay){
                if(DistPlayers(Player, Pers, Width, Height) < DistPlayers(Player, Nearest, Width, Height)){
                    Nearest = Pers;
                }
            }
        }
        return Nearest;
    }//NearestPlayer()
}

unsigned nsAI::Type1(SGame& Game, SPlayer& Player){
    return KDirections[nsUtils::Random(0, KDirections.size())];
}//nsAI::Type1()

unsigned nsAI::Type2(SGame& Game, SPlayer& Player){
    vector<unsigned> Dir = KDirections;
    
    nsUtils::Shuffle(Dir);
    
    unsigned Direction = Dir[0];
    
    for(unsigned d : Dir){
        CPosition Target = nsMap::ParseDirection(Game.Map, Player.Pos, d);
        
        MapElem TElem = nsMap::GetByCPosition(Game.Map, Target);
        
        if(TElem != 0){
            if(TElem < KMaxPlayers){ //si il y a un joueur à proximité
                Direction = d;
                break; //on fixe la position
            }
        }else
            Direction = d;
    }
    
    return Direction;
}//nsAI::Type2()

unsigned nsAI::Type3(SGame& Game, SPlayer& Player){
    vector<unsigned> Dir;
    Dir.reserve(8);
    
    unsigned Direction = 0;
    
    for(unsigned d : KDirections){ //sélectionne les directions possibles (sans traceurs)
        CPosition Target = nsMap::ParseDirection(Game.Map, Player.Pos, d);
        MapElem TElem = nsMap::GetByCPosition(Game.Map, Target);
        
        if(TElem > KMaxPlayers){ //traceur
            continue;
        }else if(TElem != 0){ //player
            Direction = d;
        }
        
        Dir.push_back(d);
    }
    
    if(Direction != 0) //direct catch
        return Direction;
    
    if(Dir.size() == 0) //entouré de traceurs => suicide obligatoire
        return KDirections[0];
    
    Direction = Dir[nsUtils::Random(0, Dir.size())];
    
    SPlayer Advers = NearestPlayer(Game, Player);
    double RealDist = DistPlayers(Player, Advers, Game.Map[0].size(), Game.Map.size());
    double MinDist = 100000;
    
    for(unsigned d : Dir){
        CPosition Target = nsMap::ParseDirection(Game.Map, Player.Pos, d);
        
        double DistAdvers = DistPlayers(Target, Advers.Pos, Game.Map[0].size(), Game.Map.size());
        
        if(RealDist < 3){ //too near
            if(DistAdvers >= RealDist && DistAdvers <= MinDist){
                Direction = d; //get the nearest secure position
                MinDist = DistAdvers;
            }
        }else if(DistAdvers < MinDist){ //suffisantly away and nearest
            Direction = d;
            MinDist = DistAdvers;
        }
    }
    
    return Direction;
}//nsAI::Type3()