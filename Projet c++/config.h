/**
 * @file   config.h
 * @author Gaëtan Perrot
 *
 * @date Created on 7 janvier 2014, 15:12
 * @brief Prototypes and variables of nsConfig namespace
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include "ui.h"


/**
 * Handle configuration
 */
namespace nsConfig{
    /**
     * Map height
     * default: 10
     */
    extern unsigned MapHeight;
    /**
     * Map width
     * default: 10
     */
    extern unsigned MapWidth;
    /**
     * internet address of server host
     * default: online.prepa-dev.fr
     */
    extern std::string OnlineHost;
    /**
     * Game port (for the online game)
     * default: 5555
     */
    extern unsigned OnlinePort;
    
    /**
     * Caracters tokens list
     */
    extern std::vector<char> PlayersToken;
    /**
     * Caracters colors list
     */
    extern std::vector<nsUI::Effect> PlayersColor;
    
    /**
     * Load the configuration
     */
    void LoadConfig();
} //nsConfig


#endif	/* CONFIG_H */

