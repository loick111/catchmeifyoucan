/** 
 * @file   datamanager.cpp
 * @author Vincent Quatrevieux
 *
 * @date 26 décembre 2013, 18:59
 * @brief functions definition of nsDataManager namespace
 */

#include <cstring>
#include "datamanager.h"
#include "packet.h"
#include "constants.h"
#include "ui.h"
#include "map.h"

using namespace nsDataManager;

std::vector<SGameRoom> nsDataManager::GetGameRooms(){
    CVPacketData Extra;
    
    try{
        Extra = nsPacket::AskList();
    }catch(...){
        throw;
    }
    
    std::vector<SGameRoom> Rooms(Extra.size());
    
    for(unsigned i = 0; i < Extra.size(); ++i){
        Rooms[i] = ParseInfoPacket(Extra[i], i + 1);
    }
    
    return Rooms;
}//nsDataManager::GetGameRooms()

SGameRoom nsDataManager::GetRoomInfo(unsigned id){
    CVPacketData Data = nsPacket::AskRoomInfo(id);
    
    if(!bool(Data[0] & ~(nsPacket::INFO << 4)) || Data.size() < 2){
        return SGameRoom();
    }
    
    return ParseInfoPacket(Data[1], id);
}//nsDataManager::GetRoomInfo()

SGameRoom nsDataManager::ParseInfoPacket(packet_data Data, unsigned id){
    SGameRoom Room;
    Room.id = id;
    Room.MaxPlayers = (Data >> 5) + 1;
    Room.CurPlayers = (Data & ~((Room.MaxPlayers - 1) << 5)) >> 1;
    Room.Accessible = Data & ~(((Room.MaxPlayers - 1) << 5) | (Room.CurPlayers << 1));
    
    return Room;
}//nsDataManager::ParseInfoPacket()

nsGame::SGame nsDataManager::ParseInitPacket(const CVPacketData& Data, unsigned& MyPid){
    unsigned i = 0;
    MyPid = Data[i++] & ~(nsPacket::INIT << 4);
    nsGame::SGame Game;

    const unsigned Width = Data[i++];
    const unsigned Height = Data[i++];
    Game.Map = nsMap::GenerateBlankMap(Width, Height);
    
    Game.Players.reserve((Data.size() - i) / 2);
    
    for(unsigned Pid = 1; i < Data.size(); i += 2){
        nsGame::SPlayer Player;
        Player.AILevel = 0;
        Player.Canplay = true;
        Player.Pid = Pid++;
        Player.Pos = CPosition(Data[i], Data[i+1]);
        nsMap::GetByCPosition(Game.Map, Player.Pos) = Player.Pid;
        Game.Players.push_back(Player);
    }
    
    nsPacket::SendInit();
    
    return Game;
} //ParseInitPacket()

SGameStats nsDataManager::ParseEndPacket(const CVPacketData& Extra, const unsigned Winner){
    SGameStats gs;
    gs.Winner = Winner;
    gs.NbPlayers = Extra[4];
    
    uint16_t Time, TurnCount;
    
    std::memcpy(&Time, &Extra[0], 2); //copy an int16_t
    std::memcpy(&TurnCount, &Extra[2], 2);
    
    gs.Time = Time;
    gs.TurnCount = TurnCount;
    
    return gs;
}