/**
 * @file   dialogs.cpp
 * @author Vincent Quatrevieux
 *
 * @date 21 décembre 2013, 12:37
 * @brief Define dialogs and other texts
 */

#include <unistd.h>
#include "ui.h"
#include "dialogs.h"
#include "game.h"
#include "constants.h"
#include "socket.h"
#include "packet.h"
#include "datamanager.h"
#include "config.h"
#include "utils.h"

void nsDialogs::MainMenu(){
    unsigned Choice = nsUI::Menu("------ CatchMe ------", {
        "Nouvelle partie",
        "En ligne",
        "Comment jouer ?",
        "Reglages",
        "Quitter"
    });
    
    switch(Choice){
        case 1:
            nsDialogs::NewGame();
            break;
        case 2:
            nsDialogs::StartOnline();
            break;
        case 3:
            nsDialogs::HowTo();
            break;
        case 4:{
            std::string Cmd = "vi " + KConfigFile;
            int s = system(Cmd.c_str());
            if(s == 0){ //Ok
                nsConfig::LoadConfig();
            }
            nsDialogs::MainMenu();
        } break;
        case 5:
            nsUI::Clear();
            exit(0);
            break;
        default:
            cout << "Work in progress..." << endl;
    }
}//nsDialogs::MainMenu()

void nsDialogs::NewGame(){
    unsigned Choice = nsUI::Menu("--- Nouvelle partie ---", {
        "Deux joueurs",
        "Trois joueurs",
        "Quatre joueurs",
        "Cinq joueurs",
        "Six joueurs",
        "Sept joueurs",
        "Huit joueurs",
        "Retour"
    });
    
    if(Choice > KMaxPlayers - 1){
        nsDialogs::MainMenu();
        return;
    }
        
    nsGame::Run(Choice + 1);
}//nsDialogs::NewGame()

void nsDialogs::ConfigPlayer(nsGame::SPlayer& Player){
    std::string Prompt = "--- Joueur ";
    Prompt += std::to_string(Player.Pid);
    Prompt += " (";
    Prompt += nsConfig::PlayersToken[Player.Pid];
    Prompt += ") ---";
    
    unsigned Choice = nsUI::Menu(Prompt, {
        "Humain",
        "IA niveau 1",
        "IA niveau 2",
        "IA niveau 3"
    });
    
    Player.AILevel = Choice - 1;
}//nsDialogs::ConfigPlayer()

void nsDialogs::EndGame(const nsGame::SGame & Game, unsigned Time, unsigned Turns){
    nsUI::Clear();
    unsigned Winner;
    unsigned Bot = 0;
    
    for(const nsGame::SPlayer & Player : Game.Players){
        if(Player.Canplay)
            Winner = Player.Pid;
        
        if(Player.AILevel > 0)
            ++Bot;
    }
    
    string Caption = "------ Joueur ";
    Caption += std::to_string(Winner);
    Caption += " vainqueur ! ------";
    
    nsUI::Table({
        {"Nombre de joueurs", std::to_string(Game.Players.size())},
        {"Nombre d'humains / bots", std::to_string(Game.Players.size() - Bot) + " / " + std::to_string(Bot)},
        {"Temps de jeu", std::to_string(Time) + " secondes"},
        {"Nombre de tours", std::to_string(Turns) + " tours"}
    }, Caption);
    
    cout << "Appuyez sur une touche pour continuer..."; 
    nsUI::KeyDown();
    
    nsDialogs::MainMenu();
}//nsDialogs::EndGame()

void nsDialogs::StartOnline(){
    nsUI::Clear();
    
    if(!nsSocket::Init()){
        if(nsDialogs::SocketError({"Serveur inaccessible"}))
            nsDialogs::StartOnline();
        else
            nsDialogs::MainMenu();
        return;
    }
    try{
        for(;;){
            nsUI::Display("Récupération de la liste des parties...", nsUI::YELLOW);
            cout << endl;

            std::vector<nsDataManager::SGameRoom> Rooms = nsDataManager::GetGameRooms();
            std::vector<std::string> Menu;

            Menu.reserve(Rooms.size() + (Rooms.size() < KMaxOnlineRooms ? 4 : 3));
            
            if(Rooms.size() < KMaxOnlineRooms)
                Menu.push_back("--- Nouvelle partie ---");

            for(const nsDataManager::SGameRoom & Room : Rooms){
                std::string Str = "Chambre " + std::to_string(Room.id) + ' ' + std::to_string(Room.CurPlayers) + '/' + std::to_string(Room.MaxPlayers);
                if(!Room.Accessible)
                    Str += " (ferme)";
                Menu.push_back(Str);
            }

            Menu.push_back("Actualiser");
            Menu.push_back("Ping");
            Menu.push_back("Deconnexion");

            unsigned Choice = nsUI::Menu("Bienvenue !", Menu);

            if(Choice == Menu.size()){ //déconnexion
                break;
            }else if(Choice == 1 && Rooms.size() < KMaxOnlineRooms){ //Nouvelle partie
                nsDialogs::NewOnline();
            }else if(Choice == Menu.size() - 2){ //actualiser
                continue;
            }else if(Choice == Menu.size() - 1){ //Ping
                nsDialogs::Ping();
            }else{
                nsDialogs::RoomInfo(Choice - (Rooms.size() < KMaxOnlineRooms ? 2 : 1));
            }
        }
    }catch(const std::exception & e){
        if(nsDialogs::SocketError(e.what())){
            nsSocket::Close();
            nsDialogs::StartOnline();
        }
    }
    nsSocket::Close();
    nsDialogs::MainMenu();
}//nsDialogs::StartOnline()

void nsDialogs::NewOnline(){
    unsigned NbPlayers = nsUI::Menu("--- Nouvelle partie ---", {
        "Deux joueurs",
        "Trois joueurs",
        "Quatres joueurs",
        "Cinq joueurs",
        "Six joueurs",
        "Sept joueurs",
        "Huit joueurs",
        "Retour"
    }) + 1;
    
    if(NbPlayers > KMaxPlayers)
        return;
    
    nsUI::Display("Création de la partie...", nsUI::YELLOW);
    cout << endl;
    
    if(!nsPacket::NewRoom(NbPlayers)){
        if(nsUI::Menu("Echec de la création...", {"Ok", "Reessayer"}) == 2)
            NewOnline();
    }
}//nsDialogs::NewOnline()

bool nsDialogs::SocketError(const std::string& Msg){
    const std::vector<std::string> Msgs = {"--- Erreur de connexion ---", Msg};
    return nsUI::Menu(Msgs, {"Ok", "Se reconnecter"}) == 2;
}//nsDialogs::socketError()

void nsDialogs::RoomInfo(unsigned id){
    nsUI::Display("Chargement des informations...", nsUI::YELLOW);
    cout << endl;
    nsDataManager::SGameRoom Room = nsDataManager::GetRoomInfo(id);
    
    if(Room.MaxPlayers == 0) //room not found
        return;
    
    std::vector<std::string> Menu;
    Menu.reserve(Room.Accessible ? 2 : 1);
    
    if(Room.Accessible)
        Menu.push_back("Rejoindre");
    Menu.push_back("Retour");
    
    unsigned Choice = nsUI::Menu({
        "Information", 
        "Chambre " + std::to_string(Room.id), 
        "Nombre de joueur(s) : " + std::to_string(Room.CurPlayers),
        "Nombre total : " + std::to_string(Room.MaxPlayers)
    }, Menu);
    
    if(Choice == 2 || !Room.Accessible)
        return;
    
    nsUI::Display("Chargement...", nsUI::YELLOW);
    cout << endl;
    if(!nsPacket::Join(Room.id)){
        unsigned Choice = nsUI::Menu("Impossible de rejoindre la chambre", {"Ok", "Reessayer"});
        
        if(Choice == 1)
            return;
        
        RoomInfo(id);
        return;
    }
    
    CVPacketData Data;
    while(nsPacket::WaitPlayers(Data)){
        nsDataManager::SGameRoom Room = nsDataManager::ParseInfoPacket(Data[1], Room.id);
        nsUI::Clear();
        nsUI::Display("En attente...\nIl y a actuellement ", nsUI::YELLOW);
        nsUI::Display(Room.CurPlayers, nsUI::GREEN);
        nsUI::Display(" joueur(s) sur ", nsUI::YELLOW);
        nsUI::Display(Room.MaxPlayers, nsUI::RED);
        cout << endl;
    }
    
    nsUI::Display("Partie créé !", nsUI::GREEN);
    cout << endl;
    
    unsigned MyPid;
    nsGame::SGame Game = nsDataManager::ParseInitPacket(Data, MyPid);
    nsGame::RunOnline(Game, MyPid);
}//nsDialogs::RoomInfo()

void nsDialogs::HowTo(){
    unsigned Page = 1;
    while(Page > 0 && Page < 5){
        std::vector<std::string> Content;
        switch(Page){
            case 1: 
                Content = {"Regles", "Le but du jeu est simple", "Il faut 'manger' le ou les adversaires.", "Pour cela vous allez vous deplacer sur la grille", "en 'tour par tour',", "afin de vous diriger vers l'adversaire.", "Le dernier survivant gagne la partie.", "A chaque deplacement, votre traceur apparaitra sur la grille.", "/!\\ ATTENTION /!\\", "Si vous franchissez un traceur, le votre egalement, vous serez elimine."};
                break;
            case 2:
                Content = {"Commandes", "Pour vous deplacer, vous utiliserez 8 touches possibles.", "Z,z, vous permet de vous deplacer vers le haut", "X,x,S,s vous permet de vous deplacer vers le bas", "Q,q, vous permet de vous deplacer vers la gauche", "D,d, vous permet de vous deplacer vers la droite", "A,a, vous permet de vous deplacer en diagonale, en haut, a gauche", "E,e, vous permet de vous deplacer en diagonale, en haut, a droite", "W,w, vous permet de vous deplacer en diagonale, en bas, a gauche", "C,c, vous permet de vous deplacer en diagonale, en bas, a droite"};
                break;
            case 3:
                Content = {"Le mode Online", "Ce jeu dispose du mode Online, ou En Ligne.", "Il vous permet de jouer avec des amis a vous au moyen d une connection internet.", "Pour jouer, il vous suffit de:", "Selectionner 'Nouvelle Partie',", "Le nombre de joueurs souhaites,", "", "Pour rejoindre une partie,", "il vous suffit de choisir la chambre correspondante", "a celle cree par votre ami."};
                break;
            case 4:
                Content = {"Credits", "Ce jeu a ete cree par :", "  -Vincent Quatrevieux", "  -Loick Mahieux", "  -Gaetan Perrot", "  -Thomas Maceri", "Merci d'avoir joue,", "Nous esperons que vous avez apprecie", "ce jeu propose par les enseignants de l'IUT d'Aix-En-Provence."};
                break;
        }
        unsigned Choice = nsUI::Menu(Content,{"Suivant", "Precedent"});
        if (Choice == 1)
            ++Page;
        else
            --Page;
    }
    nsDialogs::MainMenu();
}//nsDialogs::HowTo()

void nsDialogs::Ping(){
    nsUI::Clear();
    unsigned Nb = 0, Size = 0;
    
    do{
        cout << "Nombre de requêtes ? [1-20] ";
        cin >> Nb;
    }while(Nb == 0 || Nb > 20);
    
    do{
        cout << "Taille des packets ? [1-64] ";
        cin >> Size;
    }while(Size == 0 || Size > 64);
    
    cout << endl;
    
    vector<unsigned long> PingStats(Nb);
    unsigned long Avg = 0;
    unsigned fails = 0;
    
    for(unsigned long & Time : PingStats){
        unsigned long Start = nsUtils::Microtime();
        nsUI::Display("Envoie d'un packet HELLO de taille " + std::to_string(Size) + " : ", nsUI::YELLOW);
        cout.flush();
        
        bool Success = nsPacket::Ping(Size);
        Time = (nsUtils::Microtime() - Start) / 1000;
        if(Success){
            nsUI::Display("Ok en " + std::to_string(Time) + "ms", nsUI::GREEN);
        }else{
            nsUI::Display("Echec !", nsUI::RED);
            ++fails;
        }
        cout << endl;
        
        Avg += Time;
    }
    
    cout << endl;
    
    Avg /= Nb;
    
    unsigned long Min = 10000000;
    unsigned long Max = 0;
    
    for(unsigned long Time : PingStats){
        if(Time > Max)
            Max = Time;
        
        if(Time < Min)
            Min = Time;
    }
    
    nsUI::Table({
        {"Taux d'echec", std::to_string(fails) + "/" + std::to_string(Nb) + " (" + std::to_string(fails * 100 / Nb) + "%)"},
        {"Min", std::to_string(Min) + "ms"},
        {"Max", std::to_string(Max) + "ms"},
        {"Moyenne", std::to_string(Avg) + "ms"}
    }, "Statistiques");
    
    nsUI::Display("Appuyez sur une touche pour continuer...", nsUI::YELLOW);
    cout << endl;
    nsUI::KeyDown();
    nsUI::KeyDown();
} //Ping()

void nsDialogs::OnlineGameStats(const nsDataManager::SGameStats& gs){
    nsUI::Clear();
    
    string Caption = "------ Joueur ";
    Caption += std::to_string(gs.Winner);
    Caption += " vainqueur ! ------";
    
    nsUI::Table({
        {"Nombre de joueurs", std::to_string(gs.NbPlayers)},
        {"Temps de jeu", std::to_string(gs.Time) + " secondes"},
        {"Nombre de tours", std::to_string(gs.TurnCount) + " tours"}
    }, Caption);
    
    cout << "Appuyez sur une touche pour continuer..."; 
    nsUI::KeyDown();
} //OnlineGameStats()

