/** 
 * @file   tests.cpp
 * @author Vincent Quatrevieux
 *
 * @date 22 décembre 2013, 15:07
 * @brief Define functions of nsTests namespace
 */

#include <iostream>
#include <cassert>
#include <ctime>
#include "tests.h"
#include "game.h"
#include "map.h"
#include "constants.h"
#include "socket.h"
#include "packet.h"

/** Prepare the test*/
#define START_TEST cout << __FUNCTION__ << ": "; cout.flush(); cout.clear(ios::failbit);
/** Use after the test*/
#define END_TEST   cout.clear(ios::goodbit); cout << "Ok !" << endl;

using namespace nsTests;
using namespace std;

void nsTests::StartTests(){
    cout << "Auto-test..." << endl;
    TestMapInit();
    TestGameCanPlay();
    TestGetPosition();
    TestParseDirection();
    TestMoveToken();
    TestShowMapPerf();
    TestTablePerf();
    TestMapInitPerf();
    TestSocketPerf();
    
    cout << "Appuyez sur une touche pour continuer...";
    nsUI::KeyDown();
}//nsTests::StartTests()

void nsTests::TestMapInit(){
    START_TEST
    
    cout.flush();
    
    nsGame::SGame Game;
    
    Game.Players.resize(KNbPlayers);
    
    nsMap::Init(Game, KWidth, KHeight);
    assert(Game.Map.size() == KHeight);
    assert(Game.Map[0].size() == KWidth);
    assert(Game.Players.size() == KNbPlayers);
    
    for(unsigned i = 0; i < KNbPlayers; ++i){
        for(unsigned j = 0; j < KNbPlayers; ++j){
            if(i == j)
                continue;
            
            assert(Game.Players[i].Pos.first != Game.Players[j].Pos.first || Game.Players[i].Pos.second != Game.Players[j].Pos.second);
        }
    }
    
    END_TEST
}//nsTests::TestMapInit()

void nsTests::TestGameCanPlay(){
    START_TEST
            
    nsGame::SGame Game;
    
    const unsigned KNbPlayers = 4;
    
    Game.Players.resize(KNbPlayers);
    
    assert(nsGame::CanPlay(Game) == true);
    
    Game.Death = KNbPlayers;
    
    assert(nsGame::CanPlay(Game) == false);
    
    --Game.Death;
    
    assert(nsGame::CanPlay(Game) == false);
    
    --Game.Death;
    
    assert(nsGame::CanPlay(Game) == true);
    
    END_TEST
}//nsTests::TestGameCanPlay()

void nsTests::TestGetPosition(){
    START_TEST
            
    nsGame::SGame Game;
    Game.Players.resize(KNbPlayers);
    
    for(MapElem i = 0; i < KNbPlayers; ++i)
        Game.Players[i].Pid = i + 1;
    
    nsMap::Init(Game, KWidth, KHeight);
    
    for(const nsGame::SPlayer & Player : Game.Players){
        assert(nsMap::GetByCPosition(Game.Map, Player.Pos) == Player.Pid);
    }
    
    END_TEST
}//nsTests::TestGetPosition()

void nsTests::TestParseDirection(){
    START_TEST
    
    nsGame::SGame Game;
    
    nsMap::Init(Game, KWidth, KHeight);
    
    const CPosition PosTL(0, 0);
    
    CPosition Pos;
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::DOWN);
    assert(Pos.first == 1 && Pos.second == 0);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::RIGHT);
    assert(Pos.first == 0 && Pos.second == 1);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::DOWN | nsMap::Direction::RIGHT);
    assert(Pos.first == 1 && Pos.second == 1);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::LEFT);
    assert(Pos.first == 0 && Pos.second == KWidth - 1);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::UP);
    assert(Pos.first == KHeight - 1 && Pos.second == 0);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::DOWN | nsMap::Direction::LEFT);
    assert(Pos.first == 1 && Pos.second == KWidth - 1);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::UP | nsMap::Direction::RIGHT);
    assert(Pos.first == KHeight - 1 && Pos.second == 1);
    
    Pos = nsMap::ParseDirection(Game.Map, PosTL, nsMap::Direction::UP | nsMap::Direction::LEFT);
    assert(Pos.first == KHeight - 1 && Pos.second == KWidth - 1);
    
    END_TEST
}//nsTests::TestParseDirection()

void nsTests::TestMoveToken(){
    START_TEST
    
    nsGame::SGame Game;
    Game.Players.resize(1);
    nsGame::SPlayer & Player = Game.Players[0];
    Player.Pid = 1;
    
    nsMap::Init(Game, KWidth, KHeight);
    
    const std::vector<unsigned> KDirections = {
        nsMap::Direction::UP,
        nsMap::Direction::LEFT,
        nsMap::Direction::DOWN,
        nsMap::Direction::RIGHT,
        nsMap::Direction::UP | nsMap::Direction::LEFT,
        nsMap::Direction::UP | nsMap::Direction::RIGHT,
        nsMap::Direction::DOWN | nsMap::Direction::LEFT,
        nsMap::Direction::DOWN | nsMap::Direction::RIGHT
    };
    
    CPosition LastPos = Player.Pos;
    
    for(unsigned Dir : KDirections){
        LastPos = nsMap::ParseDirection(Game.Map, LastPos, Dir);
        nsMap::MoveToken(Game.Map, Player, Dir);
        
        assert(LastPos.first == Player.Pos.first && LastPos.second == Player.Pos.second);
    }
    
    END_TEST
}//nsTests::TestMoveToken()

void nsTests::TestShowMapPerf(){
    START_TEST
    
    nsGame::SGame Game;
    Game.Players.resize(KNbPlayers);
    
    for(MapElem i = 0; i < KNbPlayers; ++i){
        Game.Players[i].Pid = i + 1;
    }
    
    nsMap::Init(Game, KWidth, KHeight);
    
    unsigned Time = std::time(NULL);
    unsigned Count = 0;
    
    while(std::time(NULL) - Time < KTestTime){
        nsUI::ShowMap(Game.Map);
        ++Count;
    }
    
    float Fps = (float)Count / KTestTime;
    
    END_TEST
    
    cout << Count << " itérations en " << KTestTime << "sec" << endl;
    cout << "fps: " << Fps << endl;
}//nsTests::TestShowMapPerf()

void nsTests::TestTablePerf(){
    START_TEST
    
    const unsigned Time = std::time(NULL);
    unsigned Count = 0;
    
    const std::vector<std::vector<std::string> > Table = {
        {"Lorem ipsum dolor sit amet", "consectetur adipiscing elit", "Morbi pellentesque diam nec diam congue tempus"},
        {"Maecenas pharetra nisl vel odio tincidunt aliquet", " Pellentesque vitae eros", "non tortor commodo varius pretium vel elit"},
        {"Donec elementum varius lacus tincidunt scelerisque", "Duis ut pretium erat, ac vehicula eros", "Morbi a risus vestibulum"}
    };
    
    const std::string Caption = "Cum sociis natoque penatibus et magnis dis parturient montes";
    
    while(std::time(NULL) - Time < KTestTime){
        nsUI::Table(Table, Caption);
        ++Count;
    }
    
    float Fps = (float)Count / KTestTime;
    
    END_TEST
    
    cout << Count << " itérations en " << KTestTime << "sec" << endl;
    cout << "fps: " << Fps << endl;   
}

void nsTests::TestMapInitPerf(){
    START_TEST
    
    nsGame::SGame Game;
    Game.Players.resize(KNbPlayers);
    
    for(MapElem i = 0; i < KNbPlayers; ++i){
        Game.Players[i].Pid = i + 1;
    }
    
    unsigned Time = std::time(NULL);
    unsigned Count = 0;
    
    while(std::time(NULL) - Time < KTestTime){
        nsGame::SGame Tmp = Game;
        nsMap::Init(Tmp, KWidth, KHeight);
        ++Count;
    }
    
    float Fps = (float)Count / KTestTime;
    
    END_TEST
    
    cout << Count << " itérations en " << KTestTime << "sec" << endl;
    cout << "ops: " << Fps << endl;
}//nsTests::TestMapInitPerf()

void nsTests::TestSocketPerf(){
    START_TEST
    
    assert(nsSocket::Init() == true);
    
    unsigned Time = std::time(NULL);
    unsigned Count = 0;
    
    while(std::time(NULL) - Time < KTestTime){
        CVPacketData Data = nsPacket::Generate(nsPacket::HELLO);
        assert(Data.size() == 1);
        assert(Data[0] == (packet_data)0x10);
        assert(nsSocket::Write(Data) == true);
        
        Data.clear();
        assert(nsSocket::Read(Data, 1) == true);
        assert(Data.size() == 1);
        assert(Data[0] == (packet_data)0x10);
        ++Count;
    }
    
    float Ping = 1000 / ((float)Count / KTestTime);
    nsSocket::Close();
    
    END_TEST
    
    cout << Count << " requêtes en " << KTestTime << "sec" << endl;
    cout << "ping: " << Ping << "ms" << endl;
}//nsTests::TestnsSocketPerf()

void nsTests::TestAIType3(){
    START_TEST
    
    for(unsigned i = 0;i < 100; ++i){
        nsGame::SGame Game;
        Game.Players.resize(KNbPlayers);
        
        unsigned Pid = 0;
        for(nsGame::SPlayer & Player : Game.Players){
            Player.AILevel = 3;
            Player.Canplay = true;
            Player.Pid = ++Pid;
        }
        
        nsMap::Init(Game, KWidth, KHeight);
        
        while (nsGame::CanPlay(Game)) {
            for (nsGame::SPlayer & Player : Game.Players) {
                if (!Player.Canplay) //si il ne peux pas jouer, on passe son tour
                    continue;

                nsGame::StartTurn(Game, Player);

                if(!nsGame::CanPlay(Game))
                    break;
            }
        }
    }
    
    END_TEST
}

#undef START_TEST
#undef END_TEST