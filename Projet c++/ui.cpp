/**
 * @file   ui.cpp
 * @author Vincent Quatrevieux
 *
 * @date 19 décembre 2013, 13:08
 * @brief Define functions of nsUI namespace
 */

#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include "ui.h"
#include "utils.h"
#include "config.h"

using namespace std;
using namespace nsUI;

void nsUI::Clear() {
    cout << "\033[H\033[2J";
}//nsUI::Clear()

void nsUI::ShowMap(const CMap & Map) {
    Clear();
    
    BorderBox(Map[0].size() * 4 - 1);
    
    for(unsigned i = 0; i < Map.size() -1; ++i){
        // line map
        for(unsigned j = 0; j < Map[i].size(); ++j){
            // border left
            cout << '|';
            if(Map[i][j] <= KMaxPlayers) {
                Display(KEmpty, Effect::BLACK);
                Display(nsConfig::PlayersToken[Map[i][j]], nsConfig::PlayersColor[Map[i][j]]);
                Display(KEmpty, Effect::BLACK);
            }else{
                Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[i][j] - KMaxPlayers]);
                Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[i][j] - KMaxPlayers]);
                Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[i][j] - KMaxPlayers]);
            }
        }
        // border right
        cout << '|' << endl;
        
        // line inter
        // border left
        cout << '|';
        for(unsigned j = 0; j < Map[i].size() - 1; ++j){
            cout << "---+";
        }
        cout << "---";
        // border right
        cout << '|' << endl;
    }
    // last line
    for(unsigned j = 0; j < Map[Map.size() - 1].size(); ++j){
        cout << '|';
        if(Map[Map.size() - 1][j] <= KMaxPlayers) {
            Display(KEmpty, Effect::BLACK);
            Display(nsConfig::PlayersToken[Map[Map.size() - 1][j]], nsConfig::PlayersColor[Map[Map.size() - 1][j]]);
            Display(KEmpty, Effect::BLACK);
        }else{
            Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[Map.size() - 1][j] - KMaxPlayers]);
            Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[Map.size() - 1][j] - KMaxPlayers]);
            Display(KEmpty, Effect::BLACK, nsConfig::PlayersColor[Map[Map.size() - 1][j] - KMaxPlayers]);
        }
    }
    cout << '|' << endl;
    
    BorderBox(Map[0].size() * 4 - 1);
}//nsUI::ShowMap()

unsigned nsUI::KeyDown() {
    struct termios oldt, newt;
    unsigned ch;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    ch = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    return ch;
}//nsUI::KeyDown()

unsigned nsUI::Menu(const std::vector<std::string>& Msg, const std::vector<std::string>& Menu){
    unsigned Choice = 1;
    
    unsigned const MenuWidth = nsUtils::MaxLength(Menu);
    unsigned const MsgWidth = nsUtils::MaxLength(Msg);
    unsigned const BoxWidth = (MenuWidth > MsgWidth ? MenuWidth : MsgWidth);
    
    for(;;){
        Clear();
        BorderBox(BoxWidth);
        
        for(unsigned i = 0; i < Msg.size(); ++i){
            if(i == 0)
                BoxLine(Msg[i], BoxWidth, Effect::BOLD, Effect::RESET, true);
            else
                BoxLine(Msg[i], BoxWidth);
        }
        
        BorderBox(BoxWidth);
        
        for(unsigned i = 0; i < Menu.size(); ++i){
            string Disp = Menu[i];
                
            while(Disp.size() < MenuWidth)
                Disp += ' ';
            
            if(i + 1 == Choice){
                BoxLine(Disp, BoxWidth, Effect::BLACK, Effect::WHITE, true);
            }else{
                BoxLine(Disp, BoxWidth, Effect::RESET, Effect::RESET, true);
            }
        }
        BorderBox(BoxWidth);
        
        for(unsigned i = 0; i < 3; ++i){
            unsigned Key = KeyDown();
            if(i == 2){ // gestion des flèches
                if(Key == 65){ //Flèche du haut
                    --Choice;
                    if(Choice < 1)
                        Choice = 1;
                }else if(Key == 66){ //flèche du bas
                    ++Choice;
                    if(Choice > Menu.size())
                        Choice = Menu.size();
                }
            }else if(Key == 10){ //appuie sur la touche entrée
                return Choice;
            }if(i == 0 && (Key != 27 && Key != 91)){
                break;
            }
        }
    }
}//nsUI::Menu()

unsigned nsUI::Menu(const std::string& Prompt, const std::vector<std::string>& Menu){
    const vector<string> V = {Prompt};
    return nsUI::Menu(V, Menu);
}//nsUI::menu()

void nsUI::BorderBox(unsigned Size){
    cout << '+';
    for(unsigned i = 0; i < Size; ++i){
        cout << '-';
    }
    cout << '+' << endl;
}//nsUI::BorderBox()

void nsUI::BoxLine(const string& Disp, unsigned Boxsize, Effect Color/* = Effect::RESET*/, Effect Background/* = Effect::RESET*/, bool Center/* = false*/){
    
    if(Boxsize < Disp.size())
        throw "boxsize should be big than disp size";
    
    cout << '|';
    
    unsigned const NbSpaces = (Boxsize - Disp.size()) / (Center ? 2 : 1);
    
    if(Center){
        for(unsigned i = 0; i < NbSpaces; ++i)
            cout << ' ';
    }
    
    Display(Disp, Color, Background);
    
    for(unsigned i = 0; i < (Center ? Boxsize - NbSpaces - Disp.size() : NbSpaces); ++i)
        cout << ' ';
    cout << '|' << endl;
}//nsUI::BoxLine()

void nsUI::Table(const vector<vector<string> >& Table, const string& Caption){      
    vector<unsigned> ColSizes;
    
    for(unsigned i = 0; i < Table.size(); ++i){
        for(unsigned j = 0; j < Table[i].size(); ++j){
            unsigned Length = Table[i][j].length();
            if(ColSizes.size() <= j)
                ColSizes.push_back(Length);
            else if(ColSizes[j] < Length)
                ColSizes[j] = Length;
        }
    }
    
    unsigned Length = 0;
    for(unsigned ColSize : ColSizes){
        Length += ColSize + 1;
    }
    
    const unsigned BoxWidth = Caption.size() > --Length ? Caption.size() : Length;
    
    if(Length < BoxWidth){
        ColSizes[ColSizes.size() - 1] += BoxWidth - Length;
    }
    
    BorderBox(BoxWidth);
    BoxLine(Caption, BoxWidth, Effect::BOLD, Effect::RESET, true);
    
    BorderTable(ColSizes);
    
    for(const vector<string> & Line : Table){
        string StrLine;
        
        for(unsigned i = 0; i < Line.size(); ++i){
            string Cell = Line[i];
            StrLine += Cell;
            
            for(unsigned j = 0; j < ColSizes[i] - Cell.length(); ++j)
                StrLine += ' ';
            
            if(i < Line.size() - 1)
                StrLine += '|';
        }
        
        BoxLine(StrLine, BoxWidth);
    }
    
    BorderTable(ColSizes);
}//nsUI::Table()

void nsUI::BorderTable(const std::vector<unsigned>& ColSizes){   
    cout << '+';
    
    for(unsigned Size : ColSizes){
        for(unsigned i = 0; i < Size; ++i)
            cout << '-';
        cout << '+';
    }
    cout << endl;
}//nsUI::BorderTable()