/** 
 * @file   utils.hpp
 * @author Vincent Quatrevieux
 *
 * @date december 21st 2013, 14:29
 * @brief body of nsUtils functions with templates
 */

#ifndef UTILS_HPP
#define	UTILS_HPP

#include <algorithm>

#include "utils.h"

template<typename T>
void nsUtils::Shuffle(std::vector<T> & V){
    for(unsigned i = 0; i < V.size(); ++i){
        std::swap(V[i], V[nsUtils::Random(0, V.size())]);
    }
}//nsUtils::Shuffle()

template<typename T>
void nsUtils::PushAll(std::vector<T>& Vector, const std::vector<T>& Values){
    Vector.reserve(Vector.size() + Values.size());
    
    for(const T Val : Values)
        Vector.push_back(Val);
}//PushAll()

#endif	/* UTILS_HPP */

