/** 
 * @file   tests.h
 * @author Vincent Quatrevieux
 *
 * @date december 22nd 2013, 15:07
 * @brief Prototypes of nsTests namespace functions
 */

#ifndef TESTS_H
#define	TESTS_H

/**
 * Tests functions
 */
namespace nsTests{
    const unsigned KWidth = 20;
    const unsigned KHeight = 20;
    const unsigned KNbPlayers = 4;
    const unsigned KTestTime = 3;
    
    /**
     * Start all the tests
     */
    void StartTests();
    /**
     * Test nsMap::Init()
     * @see nsMap::Init
     */
    void TestMapInit();
    /**
     * test nsGame::CanPlay()
     * @see nsGame::CanPlay
     */
    void TestGameCanPlay();
    /**
     * test nsMap::MoveToken()
     * @see nsMap::MoveToken
     */
    void TestMoveToken();
    /**
     * test nsMap::GetByCPosition()
     * @see nsMap::GetByCPosition
     */
    void TestGetPosition();
    /**
     * test nsMap::ParseDirection()
     * @see nsMap::ParseDirection
     */
    void TestParseDirection();
    /**
     * test performances of nsUI::ShowMap()
     * @see nsUI::ShowMap
     */
    void TestShowMapPerf();
    /**
     * test performances of nsUI::Table()
     * @see nsUI::Table
     */
    void TestTablePerf();
    /**
     * test performances of nsMap::Init()
     * @see nsMap::Init
     */
    void TestMapInitPerf();
    /**
     * The the socket performances
     */
    void TestSocketPerf();
    /**
     * Test the AI Type3 (for seg fault)
     */
    void TestAIType3();
}//nsTests

#endif	/* TESTS_H */

