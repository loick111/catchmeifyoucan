/** 
 * @file   socket.cpp
 * @author Vincent Quatrevieux
 *
 * @date 26 décembre 2013, 16:43
 * @brief Define functions of nsSocket namespace, and save the server socket
 */

#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <cstring>
#include "socket.h"
#include "ui.h"
#include "constants.h"
#include "packet.h"
#include "config.h"

using namespace nsSocket;

namespace{
    SOCKET Sock;
}

bool nsSocket::Init(){
    nsUI::Display("Création de la connexion... ", nsUI::YELLOW);
    cout << endl;
    Sock = socket(AF_INET, SOCK_STREAM, 0);
    
    if(Sock == -1){
        nsUI::Display("Impossible de créer le socket !", nsUI::RED);
        return false;
    }
    
    struct sockaddr_in Sin;
    
    struct hostent *Host = NULL;
    Host = gethostbyname(nsConfig::OnlineHost.c_str());
    
    if(Host == NULL){
        nsUI::Display("Impossible de trouver l'hôte !", nsUI::RED);
        return false;
    }
    
    Sin.sin_addr = *(struct in_addr*) Host->h_addr;
    Sin.sin_family = AF_INET;
    Sin.sin_port = htons(nsConfig::OnlinePort);
    
    if(connect(Sock,(struct sockaddr *) &Sin, sizeof Sin) == -1){
        nsUI::Display("Connexion impossible !", nsUI::RED);
        return false;
    }
    
    nsUI::Display("Attente d'une réponse...", nsUI::YELLOW);
    cout << endl;
    
    int n;
    packet_data Data[1];
    if((n = recv(Sock, Data, 1, 0)) < 1 || !nsPacket::Corresponds(Data, nsPacket::HELLO)){
        nsUI::Display("Réponse invalide !", nsUI::RED);
        return false;
    }
    
    nsUI::Display("Connecté !", nsUI::GREEN);
    cout << endl;
    return true;
}//nsSocket::Init()

bool nsSocket::Write(const CVPacketData& Data){
    return send(Sock, &Data[0], Data.size() * sizeof(packet_data), 0) != -1;
}//nsSocket::Write()

bool nsSocket::Read(CVPacketData& Data, unsigned Size){
    Data.resize(Size);
    int n;
    if((n = recv(Sock, &Data[0], Size * sizeof(packet_data), 0)) < 1){
        return false;
    }
    
    Data.resize(n / sizeof(packet_data));
    return true;
}//nsSocket::Read()

void nsSocket::Close(){
    ::close(Sock);
    Sock = 0;
}//nsSocket::Close()
